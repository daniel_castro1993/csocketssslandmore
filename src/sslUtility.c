#include "cssm.h"

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/ec.h>
#include <openssl/pem.h>
#include <openssl/x509.h>

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define ECC_CURVE_SMALL      NID_secp128r1
#define ECC_CURVE_MEDIUM     NID_secp192k1
#define ECC_CURVE_LARGE      NID_secp384r1
#define HASH_TYPE            EVP_sha512()
#define HASH_SIZE            SHA512_DIGEST_LENGTH
#define SIGN_TYPE            HASH_TYPE
#define SYM_KEY_SIZE         1024
#define SIGN_SIZE            SYM_KEY_SIZE
#define INTERNAL_BUFFER_SIZE (SYM_KEY_SIZE<<1)
#define CERT_BUFFER_SIZE     (INTERNAL_BUFFER_SIZE<<3)
#define ECC_KEY_T(_var)      byte _var[INTERNAL_BUFFER_SIZE<<2] // TODO: their ecc_key type is broken

#define INIT_ERROR_CHECK() \
  intptr_t _err; \
//
#define ERROR_CHECK(call, teardown) \
_err = (intptr_t)(call); \
if (_err == 0 || _err == -1) { \
  BIO *_bio = BIO_new(BIO_s_mem()); \
  char *_sslError; \
  ERR_print_errors(_bio); \
  BIO_get_mem_data(_bio, &_sslError); \
  CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, _sslError); \
  BIO_free_all(_bio); \
  teardown; \
} \
//

// static EVP_PKEY *loadedPrivKey = NULL;
// static X509 *loadedCert = NULL;
// static EVP_PKEY *loadedPublKey = NULL; // this should be a x509 cert
// static unsigned char loadedSimKey[HASH_SIZE];
// static unsigned char loadedIV[HASH_SIZE];

#define GET_SECRET_LEN(_eckey) \
  ((EC_GROUP_get_degree(EC_KEY_get0_group(_eckey)) + 7) / 8) \
// ECDH_size(_eckey)
//

struct cssm_identity_t {
  EVP_PKEY *privKey;
  EVP_PKEY *publKey;
  X509_REQ *csr;
  X509 *ca;
  X509 *cert;
  unsigned char *secret;
  unsigned char *ecc_secret;
  unsigned char *secretIV;
  unsigned char *ecc_secretIV;
  EVP_PKEY *ecc_gen_key;
  EVP_PKEY *ecc_gen_key_pub;
  EC_KEY *ecc_gen_key1;
  EC_KEY *ecc_dec_key1;
};

// static void *negotiatedKeys[CSSM_MAX_SECRETS];
// static long negotiatedKeysSize = 0;
static unsigned long serial_number = 1;

static void copy_to_fields(char *str, long size, cssm_csr_fields_t *fields);
static void setFields(X509_NAME *name, cssm_csr_fields_t *fields);

cssm_identity_t *cssm_alloc_identity()
{
  cssm_identity_t *res = (cssm_identity_t*)malloc(sizeof(cssm_identity_t));
  memset(res, 0, sizeof(cssm_identity_t));
  return res;
}

void cssm_free_identity(cssm_identity_t *identity)
{
  cssm_id_destroy_keys(identity);
  free(identity);
}

// TODO: add passphrase to private key
int cssm_store_identity(
  cssm_identity_t *identity,
  const char *priv_key,
  const char *publ_key,
  const char *cert_req,
  const char *cert,
  const char *ca
) {
  FILE *priv_key_fp, *publ_key_fp, *cert_req_fp, *cert_fp, *ca_fp;
  INIT_ERROR_CHECK();

  if (identity->privKey && priv_key) {
    if ((priv_key_fp = fopen(priv_key, "wb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      return -1;
    }
    ERROR_CHECK(PEM_write_PrivateKey(priv_key_fp, identity->privKey, NULL, NULL, 0, 0, NULL),
      { fclose(priv_key_fp); return -1; });
    fclose(priv_key_fp);
  }
  
  if (identity->publKey && publ_key) {
    if ((publ_key_fp = fopen(publ_key, "wb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      return -1;
    }

    ERROR_CHECK(PEM_write_PUBKEY(publ_key_fp, identity->publKey), 
      { fclose(publ_key_fp); return -1; });
    fclose(publ_key_fp);
  }

  if (identity->csr && cert_req) {
    if ((cert_req_fp = fopen(cert_req, "wb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      return -1;
    }
    ERROR_CHECK(PEM_write_X509_REQ_NEW(cert_req_fp, identity->csr),
      { fclose(cert_req_fp); return -1; });
    fclose(cert_req_fp);
  }

  if (identity->cert && cert) {
    if ((cert_fp = fopen(cert, "wb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      return -1;
    }
    ERROR_CHECK(PEM_write_X509(cert_fp, identity->cert),
      { fclose(cert_fp); return -1; });
    fclose(cert_fp);
  }

  if (identity->ca && ca) {
    if ((ca_fp = fopen(ca, "wb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      return -1;
    }
    ERROR_CHECK(PEM_write_X509(ca_fp, identity->ca),
      { fclose(ca_fp); return -1; });
    fclose(ca_fp);
  }

  return 0;
}

int cssm_id_destroy_keys(cssm_identity_t *identity)
{
  if (identity->cert)      X509_free(identity->cert);
  if (identity->ca)        X509_free(identity->ca);
  if (identity->csr)       X509_REQ_free(identity->csr);
  if (identity->privKey && (void*)EVP_PKEY_get1_EC_KEY(identity->privKey) == (void*)identity->publKey) {
    EVP_PKEY_free(identity->privKey);
  } else {
    if (identity->publKey)   EVP_PKEY_free(identity->publKey);
    if (identity->privKey)   EVP_PKEY_free(identity->privKey);
  }
  if (identity->ecc_secret)   OPENSSL_free(identity->ecc_secret);
  if (identity->ecc_secretIV) OPENSSL_free(identity->ecc_secretIV);
  if (identity->secret)       OPENSSL_free(identity->secret);
  if (identity->secretIV)     OPENSSL_free(identity->secretIV);
  if (identity->ecc_gen_key)     EVP_PKEY_free(identity->ecc_gen_key);
  if (identity->ecc_gen_key_pub) EVP_PKEY_free(identity->ecc_gen_key_pub);
  if (identity->ecc_gen_key1)    EC_KEY_free(identity->ecc_gen_key1);
  if (identity->ecc_dec_key1)    EC_KEY_free(identity->ecc_dec_key1);
  memset(identity, 0, sizeof(cssm_identity_t));
  return 0;
}

int cssm_load_identity(
  cssm_identity_t *identity,
  const char *priv_key,
  const char *publ_key,
  const char *cert_req,
  const char *cert,
  const char *ca
) {
  FILE *priv_key_fp, *publ_key_fp, *cert_req_fp, *cert_fp, *ca_fp;
  int ret = 0;

  INIT_ERROR_CHECK();

  if (priv_key) {
    if (identity->privKey && (void*)EVP_PKEY_get1_EC_KEY(identity->privKey) == (void*)identity->publKey) {
      EVP_PKEY_free(identity->privKey);
      identity->publKey = NULL;
    } else {
      if (identity->privKey)   EVP_PKEY_free(identity->privKey);
    }
    identity->privKey = NULL;
    if ((priv_key_fp = fopen(priv_key, "rb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      ret = 1;
      goto after_priv_key;
    }
    // TODO: passphrase
    ERROR_CHECK((identity->privKey = PEM_read_PrivateKey(priv_key_fp, NULL, NULL, NULL)),
      { fclose(priv_key_fp); return -1; });
    fclose(priv_key_fp);
  }
after_priv_key:
  
  if (publ_key) {
    if (identity->publKey)   EVP_PKEY_free(identity->publKey);
    identity->publKey = NULL;
    if ((publ_key_fp = fopen(publ_key, "rb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      ret |= (1 << 1);
      goto after_publ_key;
    }

    ERROR_CHECK(identity->publKey = PEM_read_PUBKEY(publ_key_fp, NULL, NULL, NULL), 
      { fclose(publ_key_fp); return -1; });
    fclose(publ_key_fp);
  }
after_publ_key:

  if (cert_req) {
    if (identity->csr)       X509_REQ_free(identity->csr);
    identity->csr = NULL;
    if ((cert_req_fp = fopen(cert_req, "rb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      ret |= (1 << 2);
      goto after_cert_req;
    }
    ERROR_CHECK(identity->csr = PEM_read_X509_REQ(cert_req_fp, NULL, NULL, NULL),
      { fclose(cert_req_fp); return -1; });
    fclose(cert_req_fp);
  }
after_cert_req:

  if (cert) {
    if (identity->cert)      X509_free(identity->cert);
    identity->cert = NULL;
    if ((cert_fp = fopen(cert, "rb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      ret |= (1 << 3);
      goto after_cert;
    }
    ERROR_CHECK(identity->cert = PEM_read_X509(cert_fp, NULL, NULL, NULL),
      { fclose(cert_fp); return -1; });
    fclose(cert_fp);
  }
after_cert:

  if (ca) {
    if (identity->ca)        X509_free(identity->ca);
    identity->ca = NULL;
    if ((ca_fp = fopen(ca, "rb")) == NULL) {
      CSSM_ADD_ERROR("[%s:%i] %s", __FILE__, __LINE__, strerror(errno));
      ret |= (1 << 4);
      goto after_ca;
    }
    ERROR_CHECK(identity->ca = PEM_read_X509(ca_fp, NULL, NULL, NULL),
      { fclose(ca_fp); return -1; });
    fclose(ca_fp);
  }
after_ca:

  return ret;
}

int cssm_id_create_keys(cssm_identity_t *identity, int secStrength, cssm_csr_fields_t fields)
{
  int res = -1;
  EC_KEY *ecc;
  X509_REQ *x509_req;
  X509_NAME *x509_name;
  INIT_ERROR_CHECK();

  if (secStrength <= 1) {
    ecc = EC_KEY_new_by_curve_name(ECC_CURVE_SMALL);
  } else if (secStrength == 2) {
    ecc = EC_KEY_new_by_curve_name(ECC_CURVE_MEDIUM);
  } else {
    ecc = EC_KEY_new_by_curve_name(ECC_CURVE_LARGE);
  }
  EC_KEY_set_asn1_flag(ecc, OPENSSL_EC_NAMED_CURVE);

  ERROR_CHECK(EC_KEY_generate_key(ecc), { goto ret; });
  identity->privKey = EVP_PKEY_new();
  ERROR_CHECK(EVP_PKEY_assign_EC_KEY(identity->privKey, ecc),
    { EVP_PKEY_free(identity->privKey); goto ret; });
  
  identity->publKey = EVP_PKEY_new();
  ERROR_CHECK(EVP_PKEY_assign_EC_KEY(identity->publKey, 
    EVP_PKEY_get1_EC_KEY(identity->privKey)),
    { EVP_PKEY_free(identity->privKey); EVP_PKEY_free(identity->publKey); goto ret; });

  x509_req = X509_REQ_new();
  ERROR_CHECK(X509_REQ_set_version(x509_req, 1 /* version */), {
    X509_REQ_free(x509_req); EVP_PKEY_free(identity->privKey); EVP_PKEY_free(identity->publKey);
    goto ret; });
  x509_name = X509_REQ_get_subject_name(x509_req);
  X509_NAME_add_entry_by_txt(x509_name, "C" , MBSTRING_ASC, (const unsigned char*)fields.country, -1, -1, 0);
  X509_NAME_add_entry_by_txt(x509_name, "ST", MBSTRING_ASC, (const unsigned char*)fields.state, -1, -1, 0);
  X509_NAME_add_entry_by_txt(x509_name, "L" , MBSTRING_ASC, (const unsigned char*)fields.locality, -1, -1, 0);
  X509_NAME_add_entry_by_txt(x509_name, "CN", MBSTRING_ASC, (const unsigned char*)fields.commonName, -1, -1, 0);
  X509_NAME_add_entry_by_txt(x509_name, "O" , MBSTRING_ASC, (const unsigned char*)fields.org, -1, -1, 0);
  X509_NAME_add_entry_by_txt(x509_name, "OU", MBSTRING_ASC, (const unsigned char*)fields.unit, -1, -1, 0);

  ERROR_CHECK(X509_REQ_set_pubkey(x509_req, identity->privKey), { 
    X509_REQ_free(x509_req); EVP_PKEY_free(identity->privKey); EVP_PKEY_free(identity->publKey); 
    goto ret; });
  ERROR_CHECK(X509_REQ_sign(x509_req, identity->privKey, SIGN_TYPE), {
    X509_REQ_free(x509_req); EVP_PKEY_free(identity->privKey); EVP_PKEY_free(identity->publKey);
    goto ret; });
  identity->csr = x509_req;

  res = 0;
ret:
  EC_KEY_free(ecc);
  return res;
}

int cssm_id_cert_get_issuer(cssm_identity_t *identity, cssm_csr_fields_t *issuer)
{
  char *buffer;
  BIO *bo;
  long bufferSize;
  if (issuer == NULL) return -1;

  bo = BIO_new(BIO_s_mem());

  // TODO: check errors
  X509_NAME_print_ex(bo, X509_get_issuer_name(identity->cert), ',', XN_FLAG_ONELINE);
  bufferSize = BIO_get_mem_data(bo, &buffer);
  copy_to_fields(buffer, bufferSize, issuer);
  
  BIO_free_all(bo);
  return 0;
}

int cssm_id_cert_get_subject(cssm_identity_t *identity, cssm_csr_fields_t *subject)
{
  char *buffer;
  BIO *bo;
  long bufferSize;
  if (subject == NULL) return -1;
  
  bo = BIO_new(BIO_s_mem());

  // TODO: check errors
  X509_NAME_print_ex(bo, X509_get_subject_name(identity->cert), ',', XN_FLAG_ONELINE);
  bufferSize = BIO_get_mem_data(bo, &buffer);
  copy_to_fields(buffer, bufferSize, subject);

  // if (X509_NAME_oneline(X509_get_subject_name(identity->cert), buffer, 2048))

  BIO_free_all(bo);
  return 0;
}

int cssm_id_cert_check_date_valid(cssm_identity_t *identity)
{
  int ret = 0;
  time_t *ptime = NULL;

  ret = X509_cmp_time(X509_get_notBefore(identity->cert), ptime);
  ret &= X509_cmp_time(X509_get_notAfter(identity->cert), ptime);

  if (ret == -1) ret = 0;
  else if (ret == 0) ret = -1; // -1 is error

  return ret;
}

int cssm_id_create_self_signed_cert(cssm_identity_t *identity, long daysValid, cssm_csr_fields_t fields)
{
  X509_NAME *name;
  INIT_ERROR_CHECK();

  ERROR_CHECK(identity->cert = X509_new(), { return -1; });
    
  ASN1_INTEGER_set(X509_get_serialNumber(identity->cert), __sync_fetch_and_add(&serial_number, 1));
  
  X509_gmtime_adj(X509_get_notBefore(identity->cert), 0);
  X509_gmtime_adj(X509_get_notAfter(identity->cert), daysValid * 24 * 60 * 60);
  
  name = X509_get_subject_name(identity->cert);
  
  setFields(name, &fields);

  /* self signed cert issuer == subject */
  X509_set_subject_name(identity->cert, name);
  X509_set_issuer_name(identity->cert, name);
  // TODO: set isCA:TRUE flag

  X509_set_pubkey(identity->cert, identity->publKey);
  
  /* Actually sign the certificate with our key. */
  ERROR_CHECK(X509_sign(identity->cert, identity->privKey, SIGN_TYPE), { return -1; });
  identity->ca = identity->cert;
  return 0;
}

int cssm_id_cert(cssm_identity_t *ca, cssm_identity_t *csr, long daysValid)
{
  X509_NAME *subject;
  X509_NAME *issuer;
  EVP_PKEY *pkey;
  INIT_ERROR_CHECK();

  if (csr->cert) X509_free(csr->cert);

  ERROR_CHECK(csr->cert = X509_new(), { return -1; });
  ASN1_INTEGER_set(X509_get_serialNumber(csr->cert), __sync_fetch_and_add(&serial_number, 1));
  
  X509_gmtime_adj(X509_get_notBefore(csr->cert), 0);
  X509_gmtime_adj(X509_get_notAfter(csr->cert), (long) daysValid * 24 * 60 * 60);

  subject = X509_REQ_get_subject_name(csr->csr);
  issuer = X509_get_subject_name(ca->cert);
  
  // not needed: issuer is a pointer (already set)
  // cssm_csr_fields_t issuerFields;
  // cssm_id_cert_get_subject(ca, &issuerFields);
  // setFields(issuer, &issuerFields);

  /* self signed cert issuer == subject */
  X509_set_subject_name(csr->cert, subject);
  X509_set_issuer_name(csr->cert, issuer);

  pkey = X509_REQ_get_pubkey(csr->csr);
  X509_set_pubkey(csr->cert, pkey);
  // EVP_PKEY_free(pkey);
  ERROR_CHECK(X509_sign(csr->cert, ca->privKey, SIGN_TYPE), { return -1; });
  csr->ca = ca->cert;
  return 0;
}

int cssm_id_sign(cssm_identity_t *id, void *data, long dlen, void *ciphertext, long *clen)
{
  EVP_MD_CTX *mdctx = NULL;
  int ret = -1;
  size_t cSize;
  INIT_ERROR_CHECK();
  
  if (dlen == -1) dlen = strlen((const char*)data);

  ERROR_CHECK(mdctx = EVP_MD_CTX_create(), { return -1; });
  ERROR_CHECK(EVP_DigestSignInit(mdctx, NULL, SIGN_TYPE, NULL, id->privKey), { goto cssm_sign_ret; });
  ERROR_CHECK(EVP_DigestSignUpdate(mdctx, data, dlen), { goto cssm_sign_ret; });
  
  ERROR_CHECK(EVP_DigestSignFinal(mdctx, NULL, &cSize), { goto cssm_sign_ret; });
  if (cSize > CSSM_MSG_BUFFER) goto cssm_sign_ret; // does not allow overflows
  ERROR_CHECK(EVP_DigestSignFinal(mdctx, ciphertext, &cSize), { goto cssm_sign_ret; });
  if (clen) *clen = cSize;

  ret = 0;
cssm_sign_ret:
  EVP_MD_CTX_destroy(mdctx);
  return ret;
}

int cssm_id_verify(cssm_identity_t *id, void *ciphertext, long clen, void *data, long dlen)
{
  EVP_MD_CTX *mdctx;
  int ret = -1;
  INIT_ERROR_CHECK();

  if (clen == -1) clen = strlen((const char*)ciphertext);
  if (dlen == -1) dlen = strlen((const char*)data);

  ERROR_CHECK(mdctx = EVP_MD_CTX_create(), { return -1; });

  ERROR_CHECK(EVP_DigestVerifyInit(mdctx, NULL, SIGN_TYPE, NULL, id->publKey), { goto cssm_verify_ret; });
  ERROR_CHECK(EVP_DigestVerifyUpdate(mdctx, data, dlen), { goto cssm_verify_ret; });

  ret = EVP_DigestVerifyFinal(mdctx, ciphertext, clen);
cssm_verify_ret:
  EVP_MD_CTX_destroy(mdctx);
  return ret;
}

int cssm_id_set_ca(cssm_identity_t *cert, cssm_identity_t *ca)
{
  cert->ca = ca->cert;
  return 0;
}

int cssm_id_cert_verify(cssm_identity_t *cert, const char **err_str)
{
  int ret = -1;
  INIT_ERROR_CHECK();

  X509_STORE* store = X509_STORE_new();
  ERROR_CHECK(store, { return -1; });
  X509_STORE_CTX* ctx = X509_STORE_CTX_new();
  ERROR_CHECK(ctx, { goto ret_store; });

  // TODO: X509_V_FLAG_X509_STRICT fails... don't know why
  // X509_V_FLAG_X509_STRICT | X509_V_FLAG_CHECK_SS_SIGNATURE | X509_V_FLAG_POLICY_CHECK;
  static const long flags = X509_V_FLAG_CHECK_SS_SIGNATURE|X509_V_FLAG_POLICY_CHECK;
  // int rc = X509_VERIFY_PARAM_clear_flags(store, X509_V_FLAG_TRUSTED_FIRST);
  // ERROR_CHECK(rc, { goto ret_store_ctx; });
  int rc = X509_STORE_set_flags(store, flags);
  ERROR_CHECK(rc, { goto ret_store_ctx; });

  ERROR_CHECK(X509_STORE_add_cert(store, cert->ca), { goto ret_store_ctx; });

  rc = X509_STORE_CTX_init(ctx, store, cert->cert, NULL);
  ERROR_CHECK(rc, { goto ret_store_ctx; });
  
  ret = X509_verify_cert(ctx);
  int err = X509_STORE_CTX_get_error(ctx);
  if (err_str != NULL && err != 0) {
    *err_str = X509_verify_cert_error_string(err);
  }

ret_store_ctx:
  X509_STORE_CTX_free(ctx);
ret_store:
  X509_STORE_free(store);
  return ret;
}

int cssm_id_gen_ec_key(cssm_identity_t *id)
{
  cssm_identity_t *new_key;
  int res = -1;
  INIT_ERROR_CHECK();

  new_key = cssm_alloc_identity();
  cssm_id_create_keys(new_key, 1, (cssm_csr_fields_t){});
  if (new_key->csr) {
    X509_REQ_free(new_key->csr);
  }
  if (new_key->ca) {
    X509_free(new_key->ca);
  }
  if (new_key->cert) {
    X509_free(new_key->cert);
  }
  id->ecc_gen_key = new_key->privKey;
  id->ecc_gen_key_pub = new_key->publKey;

  ERROR_CHECK((id->ecc_gen_key1 = EVP_PKEY_get1_EC_KEY(id->ecc_gen_key_pub)), { goto ret; });

  res = 0;
ret:
  free(new_key);
  return res;
}

long cssm_id_serialize_ec_pubkey(cssm_identity_t *id, void *buffer, long size)
{
  BIO *bo = NULL;
  EC_KEY *key;
  long ret = -1;
  char *data;
  long dataLen;
  INIT_ERROR_CHECK();

  if (!id->ecc_gen_key1) {
    cssm_id_gen_ec_key(id);
  }
  key = id->ecc_gen_key1;

  bo = BIO_new(BIO_s_mem());
  // bo = BIO_new_mem_buf(buffer, size);
  // ERROR_CHECK(BIO_set_mem_buf(bo, ret, BIO_NOCLOSE), { ret = (void*)-1; goto ret; });

  ERROR_CHECK(PEM_write_bio_EC_PUBKEY(bo, key), { goto ret; });

  dataLen = BIO_get_mem_data(bo, &data);
  if (dataLen > size) {
    CSSM_ADD_ERROR("[ERROR]: not enough space on buffer (requires %li bytes but only %zu provided)\n", dataLen, size);
    goto ret;
  }
  memcpy(buffer, data, dataLen);

  ret = dataLen;
ret:
  BIO_free_all(bo);
  return ret;
}

int cssm_id_deserialize_ec_pubkey(cssm_identity_t *id, void *buffer, long size)
{
  BIO *bo = NULL;
  int ret = -1;
  INIT_ERROR_CHECK();

  bo = BIO_new(BIO_s_mem());
  BIO_write(bo, buffer, size);
  ERROR_CHECK(id->ecc_dec_key1 = PEM_read_bio_EC_PUBKEY(bo, NULL, NULL, NULL), { goto ret; });

  ret = 0;
ret:
  BIO_free_all(bo);
  return ret;
}

int cssm_id_gen_peer_secret(cssm_identity_t *id, cssm_identity_t *peer)
{
  if (id->ecc_gen_key1 == NULL) {
    CSSM_ADD_ERROR("[ERROR]: call first cssm_id_serialize_ec_pubkey on id\n");
    return -1;
  }
  if (peer->ecc_dec_key1 == NULL) {
    CSSM_ADD_ERROR("[ERROR]: call first cssm_id_deserialize_ec_pubkey on peer\n");
    return -1;
  }
  EC_KEY *peer_eckey = (EC_KEY*)peer->ecc_dec_key1;
  // EC_KEY *peer_eckey = (EC_KEY*) EVP_PKEY_get0_EC_KEY(peer->publKey);
  EC_KEY *local_eckey;
  const EC_POINT *public = EC_KEY_get0_public_key(peer_eckey);
  unsigned char *secret;
  long secret_size;
  int ret = -1;
  INIT_ERROR_CHECK();

  ERROR_CHECK((local_eckey = id->ecc_gen_key1), { goto ret; });
  // ERROR_CHECK((local_eckey = (void*)EVP_PKEY_get1_EC_KEY(id->privKey)), { goto ret; });

  secret_size = GET_SECRET_LEN(local_eckey);

  ERROR_CHECK((secret = OPENSSL_malloc(secret_size)), { goto ret; });
  ERROR_CHECK((secret_size = ECDH_compute_key(secret, secret_size, public, local_eckey, NULL)),
    { goto ret1; });

  if (peer->ecc_secret != NULL) {
    OPENSSL_free(peer->ecc_secret);
  }
  ERROR_CHECK((peer->ecc_secret = OPENSSL_malloc(HASH_SIZE)), { goto ret1; });
  cssm_hash(secret, secret_size, peer->ecc_secret, NULL);
  if (peer->ecc_secretIV != NULL) {
    OPENSSL_free(peer->ecc_secretIV);
  }
  ERROR_CHECK((peer->ecc_secretIV = OPENSSL_malloc(HASH_SIZE)), { goto ret1; });
  cssm_hash(peer->ecc_secret, HASH_SIZE, peer->ecc_secretIV, NULL);

  ret = 0;
ret1:
  OPENSSL_free(secret);
ret:
  return ret;
}

int cssm_id_load_secret(cssm_identity_t *id, char *secret)
{
  if (id->secret != NULL) {
    OPENSSL_free(id->secret);
  }
  if (id->secretIV != NULL) {
    OPENSSL_free(id->secretIV);
  }
  id->secret = OPENSSL_malloc(HASH_SIZE);
  id->secretIV = OPENSSL_malloc(HASH_SIZE);
  if (!secret) {
    if (!id->ecc_secret) {
      CSSM_ADD_ERROR("[ERROR]: call cssm_id_gen_peer_secret first to generate the secret\n");
      return -1;
    }
    memcpy(id->secret, id->ecc_secret, HASH_SIZE);
    memcpy(id->secretIV, id->ecc_secretIV, HASH_SIZE);
  } else {
    memcpy(id->secret, secret, HASH_SIZE);
    cssm_hash(id->secret, HASH_SIZE, id->secretIV, NULL);
  }
  return 0;
}

int cssm_id_sym_cipher(cssm_identity_t *id, void *data, long dlen, void *ciphertext, long *clen)
{
  EVP_CIPHER_CTX *ctx;
  int ret = -1;
  int len, len2;
  INIT_ERROR_CHECK();

  ERROR_CHECK((ctx = EVP_CIPHER_CTX_new()), { goto ret; });
  ERROR_CHECK(EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, id->secret, id->secretIV), { goto ret; });
  ERROR_CHECK(EVP_EncryptUpdate(ctx, ciphertext, &len, data, dlen), { goto ret; });
  ERROR_CHECK(EVP_EncryptFinal_ex(ctx, ciphertext + len, &len2), { goto ret; });
  *clen = len + len2;
  ret = 0;
ret:
  EVP_CIPHER_CTX_free(ctx);
  return ret;
}

int cssm_id_sym_decipher(cssm_identity_t *id, void *ciphertext, long clen, void *data, long *dlen)
{
  EVP_CIPHER_CTX *ctx;
  int ret = -1;
  int len, len2;
  INIT_ERROR_CHECK();

  ERROR_CHECK((ctx = EVP_CIPHER_CTX_new()), { goto ret; });
  ERROR_CHECK(EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, id->secret, id->secretIV), { goto ret; });
  ERROR_CHECK(EVP_DecryptUpdate(ctx, data, &len, ciphertext, clen), { goto ret; });
  ERROR_CHECK(EVP_DecryptFinal_ex(ctx, data + len, &len2), { goto ret; });
  *dlen = len + len2;
  ret = 0;
ret:
  EVP_CIPHER_CTX_free(ctx);
  return ret;
}

static void copy_to_str_end(char *src, char *dst)
{
  char *c = src;
  int i = 0;
  while (!(*c == ',' || *c == '\0')) {
    dst[i++] = *c;
    c++;
  }
  dst[i++] = '\0';
}

static void copy_to_fields(char *str, long size, cssm_csr_fields_t *fields)
{
  char *s = str;

  while (*s != '\0' || s - str < size) {
    while (isspace(*s)) s++;
    if (s[0] == 'C' && s[1] == ' ' && s[2] == '=')
    {
      s += 4;
      copy_to_str_end(s, fields->country);
    }
    else if (s[0] == 'L' && s[1] == ' ' && s[2] == '=')
    {
      s += 4;
      copy_to_str_end(s, fields->locality);
    }
    else if (s[0] == 'O' && s[1] == ' ' && s[2] == '=')
    {
      s += 4;
      copy_to_str_end(s, fields->org);
    }
    else if (s[0] == 'S' && s[1] == 'T' && s[2] == ' ' && s[3] == '=')
    {
      s += 5;
      copy_to_str_end(s, fields->state);
    }
    else if (s[0] == 'C' && s[1] == 'N' && s[2] == ' ' && s[3] == '=')
    {
      s += 5;
      copy_to_str_end(s, fields->commonName);
    }
    else if (s[0] == 'O' && s[1] == 'U' && s[2] == ' ' && s[3] == '=')
    {
      s += 5;
      copy_to_str_end(s, fields->unit);
    }
    else
    {
      s++;
    }
  }
}

static void setFields(X509_NAME *name, cssm_csr_fields_t *fields)
{
  if (fields->country[0] != '\0')
    X509_NAME_add_entry_by_txt(name, "C" , MBSTRING_ASC, (const unsigned char*)fields->country, -1, -1, 0);
  if (fields->state[0] != '\0')
    X509_NAME_add_entry_by_txt(name, "ST", MBSTRING_ASC, (const unsigned char*)fields->state, -1, -1, 0);
  if (fields->locality[0] != '\0')
    X509_NAME_add_entry_by_txt(name, "L" , MBSTRING_ASC, (const unsigned char*)fields->locality, -1, -1, 0);
  if (fields->commonName[0] != '\0')
    X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC, (const unsigned char*)fields->commonName, -1, -1, 0);
  if (fields->org[0] != '\0')
    X509_NAME_add_entry_by_txt(name, "O" , MBSTRING_ASC, (const unsigned char*)fields->org, -1, -1, 0);
  if (fields->unit[0] != '\0')
    X509_NAME_add_entry_by_txt(name, "OU", MBSTRING_ASC, (const unsigned char*)fields->unit, -1, -1, 0);
}

int cssm_hash(void *data, long dlen, void *hashed, long *hlen)
{
  if (hlen != NULL) {
    *hlen = SHA512_DIGEST_LENGTH;
  }
  if (data == NULL || hashed == NULL) {
    return SHA512_DIGEST_LENGTH;
  }
  SHA512_CTX sha512;
  SHA512_Init(&sha512);
  SHA512_Update(&sha512, data, dlen);
  SHA512_Final(hashed, &sha512);
  return 0;
}

int cssm_id_hmac(cssm_identity_t *id, void *data, long dlen, void *out, long *olen)
{
  unsigned int hmac_len;
  HMAC(HASH_TYPE, id->secret, HASH_SIZE, data, dlen, out, &hmac_len);
  *olen = (long)hmac_len;
  return 0;
}

static const char b64_table[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const char reverse_table[128] = {
   64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
   64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
   64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63,
   52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64,
   64,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
   15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64,
   64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
   41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64
};

int cssm_base64_encode(char *data, long dlen, char *out, long *olen)
{
  // Use = signs so the end is properly padded.
  long outpos = 0;
  long len = 0;
  int bits_collected = 0;
  unsigned int accumulator = 0;

  len = ((dlen + 2) / 3) * 4;
  memset(out, '=', len);
  out[len] = '\0';

  for (int i = 0; i < dlen; ++i) {
    accumulator = (accumulator << 8) | (data[i] & 0xffu); // char == 8 bits
    bits_collected += 8;
    while (bits_collected >= 6) {
      bits_collected -= 6;
      out[outpos++] = b64_table[(accumulator >> bits_collected) & 0x3fu];
    }
  }
  if (bits_collected > 0) { // Any trailing bits that are missing.
    accumulator <<= 6 - bits_collected;
    out[outpos++] = b64_table[accumulator & 0x3fu];
  }
  if (olen) *olen = len;
  return 0;
}

int cssm_base64_decode(char *data, long dlen, char *out, long *olen)
{
  int bits_collected = 0;
  unsigned int accumulator = 0;
  long len = 0;
  
  for (int i = 0; i < dlen; ++i) {
    const int c = data[i];
    if (isspace(c) || c == '=') {
      // Skip whitespace and padding. Be liberal in what you accept.
      continue;
    }
    if ((c > 127) || (c < 0) || (reverse_table[c] > 63)) {
      return -1;
    }
    accumulator = (accumulator << 6) | reverse_table[c];
    bits_collected += 6;
    if (bits_collected >= 8) {
      bits_collected -= 8;
      out[len++] = (accumulator >> bits_collected) & 0xffu;
    }
  }
  *olen = len;
  return 0;
}
