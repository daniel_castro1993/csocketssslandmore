#include "cssm.hpp"
#include "../deps/threading/src/util.h"
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <limits>

#define FIRST_ALLOC_SIZE 4096
#define DAYS_VALID       365

enum MSG_FLAGS {
  MSG_FLAG_HAS_HMAC         = 0b00000001,
  MSG_FLAG_IS_CIPHERED      = 0b00000010,
  MSG_FLAG_HAS_SIGNATURE    = 0b10000000 // variable sizes to the end
};

typedef struct {
  uint32_t header_size;
  uint32_t cnt_size;
  uint32_t msg_type;
  uint32_t msg_flags;
  // header stuff
  // optional HMAC
  // optional signature (variable size)
} msg_header_t;

void cssm::Message::InitBuffer()
{
  if (m_alloc_header_size == 0) {
    m_alloc_header_size = FIRST_ALLOC_SIZE;
    m_header_size = sizeof(msg_header_t);
    malloc_or_die(m_header, FIRST_ALLOC_SIZE);

    msg_header_t *header = (msg_header_t*)m_header;
    header->header_size = sizeof(msg_header_t);
    header->cnt_size = 0;
    header->msg_type = 0;
    header->msg_flags = 0;
  }
  if (m_alloc_contents_size == 0) {
    m_alloc_contents_size = FIRST_ALLOC_SIZE;
    malloc_or_die(m_contents, FIRST_ALLOC_SIZE);
    m_contents_size = 0;
  }
  if (!m_buffer) {
    m_buffer_size = CSSM_MSG_BUFFER; // max size allowed
    malloc_or_die(m_buffer, m_buffer_size);
  }
}

void cssm::Message::SetType(unsigned char type)
{
  msg_header_t *header = (msg_header_t*)m_header;
  header->msg_type = type;
}

int cssm::Message::SetContents(unsigned char *contents, long size)
{
  if (size > CSSM_MSG_BUFFER-(long)sizeof(msg_header_t)) return -1;
  if (m_contents_size < size) {
    m_buffer_size = size + m_header_size;
    do { m_alloc_contents_size <<= 1; } while (m_alloc_contents_size < size);
    m_contents_size = size;
    realloc_or_die(m_contents, m_alloc_contents_size);
  }
  msg_header_t *header = (msg_header_t*)m_header;
  header->cnt_size = size;
  memcpy(m_contents, contents, size);
  return 0;
}

int cssm::Message::SetBuffer(unsigned char *buffer, long size)
{
  unsigned char *buf = buffer;
  if (size > CSSM_MSG_BUFFER) { 
    CSSM_ADD_ERROR(
      "[ERROR]: not enough space on buffer \
      (requires %li bytes but only %i provided)\n",
      size, CSSM_MSG_BUFFER
    );
    return -1;
  }
  memcpy(m_header, buf, sizeof(msg_header_t));
  msg_header_t *header = (msg_header_t*)m_header;
  m_contents_size = header->cnt_size;
  buf = (unsigned char *)((uintptr_t)buf + sizeof(msg_header_t));
  SetContents(buf, header->cnt_size);
  return 0;
}

unsigned char cssm::Message::GetType()
{
  msg_header_t *header = (msg_header_t*)m_header;
  return header->msg_type;
}

unsigned char *cssm::Message::GetContents(long &size)
{
  size = m_contents_size;
  return m_contents;
}

unsigned char *cssm::Message::GetBuffer(long &size)
{
  msg_header_t *header = (msg_header_t*)m_header;
  size = header->cnt_size + sizeof(msg_header_t);
  memcpy(m_buffer, m_header, sizeof(msg_header_t));
  memcpy((void*)((uintptr_t)m_buffer + sizeof(msg_header_t)),
    m_contents, m_contents_size);
  return m_buffer;
}

int cssm::Message::Sign(cssm_identity_t *id)
{
  msg_header_t *header = (msg_header_t*)m_header;
  unsigned char *cpyBuffer;
  unsigned char buffer[CSSM_MSG_BUFFER];
  long sizeMsg, sizeCypher;
  if (header->msg_flags & MSG_FLAG_HAS_SIGNATURE) {
    return -1; // already signed
  }
  if (header->msg_flags & MSG_FLAG_HAS_HMAC) {
    return -2; // does not realloc the signature
  }
  cpyBuffer = GetBuffer(sizeMsg);
  // does not allow overflowss
  if (cssm_id_sign(id, cpyBuffer, sizeMsg, buffer, &sizeCypher)) return -1;
  uintptr_t sign_ptr = ((uintptr_t)m_header) + sizeof(msg_header_t);
  memcpy((void*)sign_ptr, buffer, sizeCypher);

  header->msg_flags |= MSG_FLAG_HAS_SIGNATURE;
  return 0;
}

int cssm::Message::Verify(cssm_identity_t *peer)
{
  msg_header_t *header = (msg_header_t*)m_header;
  uintptr_t start_ptr = ((uintptr_t)m_header) + sizeof(msg_header_t);
  long signature_size = header->header_size - sizeof(msg_header_t);
  if (!(header->msg_flags & MSG_FLAG_HAS_SIGNATURE)) {
    return -1; // there is no signature
  }
  if (header->msg_flags & MSG_FLAG_HAS_HMAC) {
    return -2; // not supported
  }
  return cssm_id_verify(peer, (void*)start_ptr, signature_size, m_buffer, m_buffer_size);
}

int cssm::Message::HmacSign(cssm_identity_t *id)
{
  msg_header_t *header = (msg_header_t*)m_header;
  if (header->msg_flags & MSG_FLAG_HAS_HMAC) {
    return -1; // already here
  }
  if (header->msg_flags & MSG_FLAG_HAS_SIGNATURE) {
    return -2; // does not realloc
  }
  unsigned char buffer[FIRST_ALLOC_SIZE];
  long size;
  cssm_id_hmac(id, m_buffer, m_buffer_size, buffer, &size);
  if (header->header_size + (uint32_t)size > (uint32_t)m_alloc_header_size) {
    m_alloc_header_size <<= 1;
    realloc_or_die(m_header, m_alloc_header_size);
    header = (msg_header_t*)m_header;
    m_header_size += size;
    header->header_size += size;
  }
  uintptr_t sign_ptr = ((uintptr_t)m_header) + sizeof(msg_header_t);
  memcpy((void*)sign_ptr, buffer, size);
  header->msg_flags |= MSG_FLAG_HAS_HMAC;
  return 0;
}

int cssm::Message::HmacVerify(cssm_identity_t *peer)
{
  msg_header_t *header = (msg_header_t*)m_header;
  if (header->msg_flags & MSG_FLAG_HAS_HMAC) {
    return -1; // already here
  }
  if (header->msg_flags & MSG_FLAG_HAS_SIGNATURE) {
    return -2; // does not realloc
  }
  unsigned char buffer[FIRST_ALLOC_SIZE];
  long size;
  cssm_id_hmac(peer, m_buffer, m_buffer_size, buffer, &size);
  uintptr_t sign_ptr = ((uintptr_t)m_header) + sizeof(msg_header_t);
  memcpy((void*)sign_ptr, buffer, size);

  for (int i = 0; i < CSSM_HASH_SIZE; ++i) {
    if (((uint8_t*)sign_ptr)[i] != ((uint8_t*)buffer)[i]) return 0;
  }
  return 1;
}

int cssm::Message::Cipher(cssm_identity_t *id)
{
  unsigned char *buffer;
  long size, bufferSize;
  msg_header_t *header = (msg_header_t*)m_header;
  unsigned char *contents = GetBuffer(bufferSize);

  contents = (unsigned char *)((uintptr_t)contents + sizeof(msg_header_t));

  malloc_or_die(buffer, CSSM_MSG_BUFFER);

  cssm_id_sym_cipher(id, contents, header->cnt_size, buffer, &size);

  SetContents(buffer, size);

  free(buffer);
  return 0;
}

int cssm::Message::Decipher(cssm_identity_t *id)
{
  unsigned char *buffer;
  long size;
  msg_header_t *header = (msg_header_t*)m_header;

  malloc_or_die(buffer, CSSM_MSG_BUFFER);

  cssm_id_sym_decipher(id, m_contents, header->cnt_size, buffer, &size);

  SetContents(buffer, size);
  free(buffer);
  return 0;
}

int cssm::Message::RespondWith(Message &msg)
{
  void *buffer;
  long len;
  buffer = msg.GetBuffer(len);
  cssm_err_flag = 0;
  m_respondWith(buffer, len);
  if (cssm_err_flag) throw cssm::CSSMError();
  return 0;
}

cssm::Message cssm::Message::WaitResponse()
{
  unsigned char buffer[CSSM_MSG_BUFFER];
  long len = CSSM_MSG_BUFFER;
  cssm_err_flag = 0;
  m_waitResponse(buffer, &len);
  if (cssm_err_flag) throw cssm::CSSMError();
  return cssm::Message(1, buffer, len);
}

// internal use only
int cssm::Message::SetRespondWith(void(*respondWith)(void*,long))
{
  m_respondWith = respondWith;
  return 0;
}

int cssm::Message::SetWaitResponse(void(*waitResponse)(void*,long*))
{
  m_waitResponse = waitResponse;
  return 0;
}

int cssm::IEntity::StoreId()
{
  std::string privKey = m_localPrivKeys + m_name + "_priv_key.pem";
  std::string publKey = m_localPublKeys + m_name + "_publ_key.pem";
  std::string csr     = m_localPublKeys + m_name + "_csr.pem";
  std::string cert    = m_localPublKeys + m_name + "_cert.pem";
  std::string ca      = m_localPublKeys + m_name + "_ca.pem";
  return cssm_store_identity(
    m_id,
    privKey.c_str(),
    publKey.c_str(),
    csr.c_str(),
    cert.c_str(),
    ca.c_str()
  );
}

int cssm::IEntity::LoadId()
{
  std::string privKey = m_localPrivKeys + m_name + "_priv_key.pem";
  std::string publKey = m_localPublKeys + m_name + "_publ_key.pem";
  std::string csr     = m_localPublKeys + m_name + "_csr.pem";
  std::string cert    = m_localPublKeys + m_name + "_cert.pem";
  std::string ca      = m_localPublKeys + m_name + "_ca.pem";
  return cssm_load_identity(
    m_id,
    privKey.c_str(),
    publKey.c_str(),
    csr.c_str(),
    cert.c_str(),
    ca.c_str()
  );
}

int cssm::IEntity::VerifyId(cssm_identity_t *ca)
{
  const char **err_str = NULL;
  cssm_id_set_ca(m_id, ca);
  int ret = cssm_id_cert_verify(m_id, err_str);
  if (err_str != NULL) printf("cssm::IEntity::VerifyId: %s\n", *err_str);
  return ret;
}

int cssm::IEntity::VerifyId(const char *caCert)
{
  const char **err_str = NULL;
  cssm_identity_t *ca = cssm_alloc_identity();
  cssm_load_identity(ca, NULL, NULL, NULL, caCert, NULL);
  cssm_id_set_ca(m_id, ca);
  int ret = cssm_id_cert_verify(m_id, err_str);
  if (err_str != NULL) printf("cssm::IEntity::VerifyId: %s\n", *err_str);
  return ret;
}

int cssm::RemoteEntity::PairWithKey(cssm_identity_t *pair)
{
  m_pairKey = pair;
  cssm_id_gen_peer_secret(m_id, m_pairKey);
  cssm_id_load_secret(m_id, NULL);
  return 0;
}

int cssm::RemoteEntity::CipherMsg(cssm::Message &msg)
{
  return msg.Cipher(m_id);
}

int cssm::RemoteEntity::DecipherMsg(cssm::Message &msg)
{
  return msg.Decipher(m_id);
}

int cssm::RemoteEntity::SignMsg(cssm::Message &msg)
{
  return msg.Sign(m_id);
}

int cssm::RemoteEntity::VerifyMsg(cssm::Message &msg)
{
  return msg.Verify(m_id);
}

int cssm::RemoteEntity::HmacSignMsg(cssm::Message &msg)
{
  return msg.HmacSign(m_id);
}

int cssm::RemoteEntity::HmacVerityMsg(cssm::Message &msg)
{
  return msg.HmacVerify(m_id);
}

int cssm::Connection::ConectTo(RemoteEntity re)
{
  char port[64] = "0";
  sprintf(port, "%i", re.GetPort());
  m_conn[re.GetName()] = re;
  // TODO: m_stat
  if (m_currentConn >= 0) cssm_close_all_connections(); // TODO: this closes all connections
  m_currentConn = cssm_connect_to((char*)re.GetAddr().c_str(), (char*)port);
  if (m_currentConn < 0) throw cssm::TSLConnError(-1 * m_currentConn);
  return m_currentConn;
}

int cssm::Connection::ConectTo(std::string name)
{
  auto find = m_conn.find(name);
  char port[64] = "0";
  if (find == m_conn.end()) return -1;
  if (m_currentConn > 0) cssm_close_all_connections(); // TODO: this closes all connections
  sprintf(port, "%i", find->second.GetPort());
  m_currentConn = cssm_connect_to((char*)find->second.GetAddr().c_str(), (char*)port);
  if (m_currentConn < 0) throw cssm::TSLConnError(-1 * m_currentConn);
  return m_currentConn;
}

int cssm::Connection::SendMsg(cssm::Message &msg)
{
  void *buffer;
  long len;
  if (m_currentConn < 0) return -1;

  buffer = msg.GetBuffer(len);
  cssm_send_msg(m_currentConn, buffer, len);
  return 0;
}

static int isHandlerSet = 0;
static cssm::ProcessMsg_t clbks[std::numeric_limits<unsigned char>::max()];

static void server_handler(
  void *buffer,
  long len,
  void(*respondWith)(void*,long),
  void(*waitResponse)(void*,long*)
) {
  cssm::Message msg(1, (unsigned char*)buffer, len);
  msg.SetRespondWith(respondWith);
  msg.SetWaitResponse(waitResponse);

  // TODO: use flags to auto sign/cypher

  uint32_t type = msg.GetType();
  switch (type) {
    case 0x101: // Identity export
      break;
    // case 0x1XX: // TODO: control messages 
    default:
      if (clbks[type]) clbks[type](msg);
      break;
  }
} 

int cssm::Server::Start()
{
  char port[64] = "0";
  if (m_port != -1) { sprintf(port, "%i", m_port); }
  cssm_init(port);
  m_port = cssm_check_port();
  if (!isHandlerSet) {
    cssm_add_handler(server_handler);
    isHandlerSet = 1;
  }
  return m_port;
}

int cssm::Server::AddCallback(unsigned char msgType, cssm::ProcessMsg_t clbk)
{
  int ret = clbks[msgType] != NULL;
  clbks[msgType] = clbk;
  return ret;
}

int cssm::Server::SendMsg(cssm::RemoteEntity re, cssm::Message &msg)
{
  m_conn.ConectTo(re);
  return m_conn.SendMsg(msg);
}

int cssm::Server::SendMsg(std::string name, cssm::Message &msg)
{
  m_conn.ConectTo(name);
  return m_conn.SendMsg(msg);
}

int cssm::Server::SendMsg(cssm::Message &msg)
{
  return m_conn.SendMsg(msg);
}
