#define _XOPEN_SOURCE 700
#include "cssm.h"
#include "threading.h"

#include <stdio.h> 
#include <errno.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netdb.h> 
#include <arpa/inet.h> 
#include <unistd.h>
#include <stdint.h>
#include <openssl/ssl.h>

// macOS
#ifndef SOCK_NONBLOCK
#include <fcntl.h>
# define SOCK_NONBLOCK O_NONBLOCK
#endif

__thread int cssm_err_flag;
__thread char cssm_last_error_msg[1024];

typedef struct buffer_t {
  long size;
  int connfd;
  void *ptr;
} buffer_t;

static cssm_handler_t handlers[CSSM_MAX_HANDLERS];
static int nbHandlers = 0;
static volatile int isServerOn = 0;
static volatile int isServerError = 0;
static int isExit = 0;
static volatile int isServerSet = 0;
static __thread int thrdServerPort;

__thread int clientConnections[CSSM_MAX_CONNECTIONS];
__thread int nbClientConnections = 0;
__thread buffer_t *currBuffer;

#define INIT_ERROR_CHECK() \
  intptr_t _err; \
//
#define ERROR_CHECK(call, teardown) \
_err = (intptr_t)(call); \
if (_err < 0) { \
  CSSM_ADD_ERROR("[%s:%i] %s (%i)", __FILE__, __LINE__, strerror(errno), errno); \
  teardown; \
} \
//

#ifndef __linux__
void blocking_socket_timeout(void *arg)
{
  char port[128];
  int connection;
  int nullMsg = 0;
  sprintf(port, "%i", thrdServerPort);
  connection = cssm_connect_to("127.0.0.1", port);
  cssm_send_msg(connection, &nullMsg, sizeof(int));
  // cssm_close_all_connections();
  threading_timer(1000/* each second */, blocking_socket_timeout, NULL);
}
#endif

// respond callback
static void respondWith(void *packet, long len)
{
  char sendTmpBuffer[CSSM_MSG_BUFFER]; 
  long base64Size;
  INIT_ERROR_CHECK();
  cssm_base64_encode((char*)packet, len, sendTmpBuffer, &base64Size);
  ERROR_CHECK(write(currBuffer->connfd, sendTmpBuffer, base64Size), { return; });
  fsync(currBuffer->connfd);
}

// buffer must have size CSSM_MSG_BUFFER
static void waitResponse(void *packet, long *len)
{
  char recvTmpBuffer[CSSM_MSG_BUFFER]; 
  long base64decodeSize;
  INIT_ERROR_CHECK();
  ERROR_CHECK((*len = read(currBuffer->connfd, recvTmpBuffer, CSSM_MSG_BUFFER)), { return; });
  cssm_base64_decode(recvTmpBuffer, *len, (char*)packet, &base64decodeSize);
  *len = base64decodeSize;
}

static void handler_fn(void *arg, int idx)
{
  currBuffer = (buffer_t*)arg;
  for (int i = 0; i < nbHandlers; i++) {
    handlers[i](currBuffer->ptr, currBuffer->size, respondWith, waitResponse); // waits for the handler
  }
  free(currBuffer->ptr);
  free(arg);
}

static void server(int id, int nbThreads, void *arg)
{
  char const *portZero = "0";
  char const *serverPort = (char const *)arg;
  char recvTmpBuffer[CSSM_MSG_BUFFER]; // TODO: this must be aligned 
  int retError = 1;
  int socketfd, connfd;
  long base64len;
  if (serverPort == NULL) serverPort = portZero;
  const struct sockaddr_in servaddr = {
    .sin_family = AF_INET,
    .sin_port = htons(atol(serverPort)),
    .sin_addr = { .s_addr = htonl(INADDR_ANY) }
  }, cli;
  const struct sockaddr_in servaddr_read;
  socklen_t len = sizeof(struct sockaddr);
  INIT_ERROR_CHECK();

  ERROR_CHECK((socketfd = socket(AF_INET, SOCK_STREAM
#ifdef __linux__
  |SOCK_NONBLOCK
#endif /* __linux */
  , 0)), { goto ret; });
  ERROR_CHECK(bind(socketfd, (struct sockaddr*)&servaddr, sizeof(servaddr)), { goto ret; });
  ERROR_CHECK(getsockname(socketfd, (struct sockaddr*)&servaddr_read, &len), { goto ret; });
  thrdServerPort = servaddr_read.sin_port;
  ERROR_CHECK(listen(socketfd, CSSM_MSG_QUEUE_SIZE), { goto ret; });


#ifndef __linux__
  threading_timer(1000, blocking_socket_timeout, NULL);
#endif

  __atomic_store_n(&isServerOn, 1, __ATOMIC_RELEASE);
  while (!__atomic_load_n(&isExit, __ATOMIC_ACQUIRE)) { 
    ERROR_CHECK((connfd = accept(socketfd, (struct sockaddr*)&cli, &len)), {
#ifdef __linux__
      // non-blocking sockets return EAGAIN on timeout
      if (errno != EAGAIN)
#endif
      fprintf(stderr, "%s\n", cssm_last_error_msg);
      continue;
    });

    // copy data
    void *data = malloc(sizeof(buffer_t));
    ((buffer_t*)data)->connfd = connfd;

    ERROR_CHECK((base64len = read(connfd, (void*)recvTmpBuffer, CSSM_MSG_BUFFER)), {
      fprintf(stderr, "%s\n", cssm_last_error_msg); continue;
    });

    if (base64len == 0) { // empty message...
      continue;
    }

    ((buffer_t*)data)->ptr = malloc(base64len); // allocs a bit more than needed (base64 inflates data)
    cssm_base64_decode(recvTmpBuffer, base64len, ((buffer_t*)data)->ptr, &(((buffer_t*)data)->size));

    // handle
    threading_async(0, handler_fn, data);
  }
  ERROR_CHECK(close(socketfd), { goto ret; });

  retError = 0;
ret:
  if (retError) {
    __atomic_store_n(&isServerError, 1, __ATOMIC_RELEASE);
    __atomic_store_n(&isServerSet, 0, __ATOMIC_RELEASE);
    fprintf(stderr, "ERROR %s \n", cssm_last_error_msg);
  }
}

int cssm_init(char *port)
{
  if (isServerSet) {
    // start another server

    threading_start
    ( /* just 1 thread for the server */1,
      /* handler already init */0,
      server,
      port
    );

    goto ret;
  }
  SSL_library_init();
  SSL_load_error_strings();
  isServerSet = 1;
  threading_start
  ( /* just 1 thread for the server */1,
    /* handler */CSSM_HANDLER_THREADS,
    server,
    port
  );

ret:
  while (!__atomic_load_n(&isServerOn, __ATOMIC_ACQUIRE)
      && !__atomic_load_n(&isServerError, __ATOMIC_ACQUIRE));
  if (isServerError) return -1;
  __atomic_store_n(&isServerOn, 0, __ATOMIC_RELEASE);
  return 0;
}

int cssm_check_port()
{
  return thrdServerPort;
}

int cssm_destroy()
{
  __atomic_store_n(&isExit, 1, __ATOMIC_RELEASE);
  // send msg to localhost:port to wake the server
  if (isServerSet) {
    cssm_close_all_connections();
  }

  threading_join();
  return 0;
}

int cssm_connect_to(char *addr, char *port)
{
  INIT_ERROR_CHECK();
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  int sfd = -1, s, conn;

  if (nbClientConnections >= CSSM_MAX_CONNECTIONS) {
    CSSM_ADD_ERROR("Reached maximum number of connections");
    return -1;
  }

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
  hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
  hints.ai_flags = 0;
  hints.ai_protocol = 0;          /* Any protocol */

  s = getaddrinfo(addr, port, &hints, &result);
  if (s != 0) {
    CSSM_ADD_ERROR("getaddrinfo: %s", gai_strerror(s));
    return -2;
  }

  for (rp = result; rp != NULL; rp = rp->ai_next) {
    sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if ((clientConnections[nbClientConnections] = sfd) == -1) continue;
    if ((conn = connect(sfd, rp->ai_addr, rp->ai_addrlen)) != -1) break; /* Success */
  }

  ERROR_CHECK(sfd, { sprintf(cssm_last_error_msg, "%s", strerror(errno)); return -3; });
  ERROR_CHECK(conn, { sprintf(cssm_last_error_msg, "%s", strerror(errno)); return -3; });
  nbClientConnections++;
  return nbClientConnections-1;
}

int cssm_close_all_connections()
{
  for (int i = 0; i < nbClientConnections; ++i) {
    fsync(clientConnections[i]);
    close(clientConnections[i]);
  }
  nbClientConnections = 0;
  return 0;
}

int cssm_send_msg(int connId, void *msg, long len)
{
  // takes in account base64 inflation
  if (len >= CSSM_MSG_BUFFER / 4 * 3) return -1;
  char sendTmpBuffer[CSSM_MSG_BUFFER]; 
  long base64Size;
  int n;
  INIT_ERROR_CHECK();
  cssm_base64_encode((char*)msg, len, sendTmpBuffer, &base64Size);
  ERROR_CHECK(
    (n = write(clientConnections[connId], sendTmpBuffer, base64Size)),
    { return -1; });
  if (n != len) return -2; // sent but size mismatches
  fsync(clientConnections[connId]);
  return 0;
}

int cssm_recv_msg(int connId, void *msg, long *len)
{
  char recvTmpBuffer[CSSM_MSG_BUFFER]; 
  long base64decodeSize;
  INIT_ERROR_CHECK();
  ERROR_CHECK(
    (*len = read(clientConnections[connId], recvTmpBuffer, CSSM_MSG_BUFFER)),
    { return -1; });
  cssm_base64_decode(recvTmpBuffer, *len, (char*)msg, &base64decodeSize);
  *len = base64decodeSize;
  return 0;
}

// handlers are added in order
int cssm_add_handler(cssm_handler_t handler)
{
  if (nbHandlers < CSSM_MAX_HANDLERS) {
    handlers[nbHandlers] = handler;
    nbHandlers++;
  } else {
    // error, out of space
    return -1;
  }
  return 0;
}
