#include "cssm.hpp"
#include "input_handler.h"
#include "threading.h"

#include <stdio.h>
#include <assert.h>
#include <string.h>

int port1 = 16080;
int port2 = 16081;
int port3 = 16082;

thread_local int thrd_id;

static int
process_msg(cssm::Message &msg)
{
  long msgSize;
  unsigned char *msgCnt;
  msgCnt = msg.GetContents(msgSize);
  printf("[%i] received msg %s\n", thrd_id, (char*)msgCnt);
  return 1;
}

void
node1(int id, int nb_thrs, void *arg)
{
  cssm::Entity e1("e1");
  cssm::RemoteEntity e2("e2", "localhost", port2);
  cssm::RemoteEntity e3("e3", "localhost", port3);
  cssm::Message msg1("Message from node1 to node2");
  cssm::Message msg2("Message from node1 to node3");
  cssm::Server s1(e1, port1);

  msg1.SetType(1);
  msg2.SetType(1);

  s1.AddCallback(1, process_msg);

  thrd_id = id;

  printf("[%i] started!\n", id);
  
  s1.SendMsg(e2, msg1);
  s1.SendMsg(e3, msg2);
}

void 
node2(int id, int nb_thrs, void *arg)
{
  cssm::Entity e2("e2");
  cssm::RemoteEntity e1("e1", "localhost", port1);
  cssm::RemoteEntity e3("e3", "localhost", port3);
  cssm::Message msg1("Message from node2 to node1");
  cssm::Message msg2("Message from node2 to node3");
  cssm::Server s2(e2, port2);

  thrd_id = id;

  printf("[%i] started!\n", id);

  msg1.SetType(1);
  msg2.SetType(1);

  s2.AddCallback(1, process_msg);
}

void 
node3(int id, int nb_thrs, void *arg)
{
  cssm::Entity e3("e3");
  cssm::RemoteEntity e1("e1", "localhost", port1);
  cssm::RemoteEntity e2("e2", "localhost", port2);
  cssm::Message msg1("Message from node3 to node1");
  cssm::Message msg2("Message from node3 to node2");
  cssm::Server s3(e3, port3);

  thrd_id = id;

  printf("[%i] started!\n", id);

  msg1.SetType(1);
  msg2.SetType(1);

  s3.AddCallback(1, process_msg);

  s3.SendMsg(e1, msg1);
  s3.SendMsg(e2, msg2);
}

int
main
( int argc,
  char **argv
) {
  char buffer[CSSM_MSG_BUFFER] = "hello";
  char buffer2[CSSM_MSG_BUFFER] = "bye!";
  long len;
  input_parse(argc, argv);
  if (input_exists("PORT1")) { port1 = input_getLong("PORT1"); }
  if (input_exists("PORT2")) { port2 = input_getLong("PORT2"); }
  if (input_exists("PORT3")) { port3 = input_getLong("PORT3"); }

  printf("Starting node1...\n");
  threading_start
  ( /* node thread */1,
    /* asyncs */0,
    node1,
    NULL
  );

  printf("Starting node2...\n");
  threading_start
  ( /* node thread */1,
    /* asyncs */0,
    node2,
    NULL
  );

  printf("Starting node3...\n");
  threading_start
  ( /* node thread */1,
    /* asyncs */0,
    node3,
    NULL
  );

  threading_join();
  printf("Exit now...\n");

  cssm_destroy();

  return EXIT_SUCCESS;
}

