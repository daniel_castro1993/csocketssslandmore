#include "cssm.h"
#include "input_handler.h"

#include <stdio.h>
#include <assert.h>
#include <string.h>

void handler(void *msg, long len, void(*respondWith)(void*,long), void(*waitResponse)(void*, long*))
{
  printf("[SERVER] got message: %s\n", (char*)msg);
  char buffer[CSSM_MSG_BUFFER] = "world!";
  long len2 = strlen(buffer)+1;
  respondWith(buffer, len2);
  waitResponse((void*)buffer, &len2);
  printf("[SERVER] got message: %s\n", (char*)buffer);
} 

int main (int argc, char **argv)
{
  char port[64] = "16080";
  char buffer[CSSM_MSG_BUFFER] = "hello";
  char buffer2[CSSM_MSG_BUFFER] = "bye!";
  long len;
  input_parse(argc, argv);
  if (input_exists("PORT")) {
    input_getString("PORT", port); // test size
  }

  printf("openning port %s\n", port);

  cssm_init(port);

  cssm_add_handler(handler);

  int connId = cssm_connect_to("127.0.0.1", port);
  printf("connection = %i\n", connId);
  cssm_send_msg(connId, buffer, strlen(buffer)+1); // do not forget \0
  cssm_recv_msg(connId, (void*)buffer, &len);
  cssm_send_msg(connId, buffer2, strlen(buffer2)+1);
  cssm_close_all_connections();

  printf("[CLIENT]: got message %s\n", (char*)buffer);

  cssm_destroy();

  return EXIT_SUCCESS;
}

