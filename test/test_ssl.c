#include "cssm.h"
#include "input_handler.h"

#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

int main (int argc, char **argv)
{
  char publKey[128] = "publ.pem";
  // char publKey2[128] = "publ2.pem";
  char privKey[128] = "priv.pem";
  // char privKey2[128] = "priv2.pem";
  char publKeyCsr[128] = "publ.csr";
  // char publKeyCsr2[128] = "publ2.csr";
  char publKeyCrt1[128] = "publ1.crt";
  char publKeyCrt2[128] = "publ2.crt";
  char msg[64] = "Hello world!";
  char signature[512];
  char signatureBase64[1024];
  long signatureBase64Size;
  long len, msgLen = strlen(msg) + 1, cipLen;
  cssm_csr_fields_t fields;

  cssm_init(NULL);

  input_parse(argc, argv);
  if (input_exists("PUBL")) {
    input_getString("PUBL", publKey);
  }

  if (input_exists("PRIV")) {
    input_getString("PRIV", publKey);
  }

  if (input_exists("CSR")) {
    input_getString("CSR", publKeyCsr);
  }

  if (input_exists("CRT1")) {
    input_getString("CRT1", publKeyCrt1);
  }

  if (input_exists("CRT2")) {
    input_getString("CRT2", publKeyCrt2);
  }

  printf("writing keys in %s (public) and %s (private)\n", publKey, privKey);

  cssm_identity_t *id1 = cssm_alloc_identity();
  cssm_identity_t *id2 = cssm_alloc_identity();
  
  fields = (cssm_csr_fields_t){
    .country = "PT",
    .state = "PT",
    .locality = "Lisbon",
    .org = "subjectOrg",
    .unit = "subjectOrgUnit",
    .commonName = "subject",
    .email = "subject@com.pt"
  };

  cssm_id_create_keys(id1, 1, fields);
  cssm_id_create_keys(id2, 1, fields);

  cssm_id_create_self_signed_cert(id1, 500, (cssm_csr_fields_t){
    .country = "PT",
    .state = "PT",
    .locality = "Lisbon",
    .org = "myOrganization",
    .unit = "myOrgUnit",
    .commonName = "myCommonName",
    .email = "my_email@com.pt"
  });

  len = 512;
  if (cssm_id_sign(id1, msg, msgLen, (void**)&signature, &len)) {
    printf("(id1) error signing: %s\n", cssm_last_error_msg);
  }

  cssm_base64_encode(signature, len, signatureBase64, &signatureBase64Size);
  printf("(id1) msg = %s signed to %s (len=%lu) verify = %i\n", msg,
    signatureBase64, len, cssm_id_verify(id1, signature, len, msg, msgLen));
  
  printf("cssm_cert_get_issuer(id1, ...)\n");
  cssm_id_cert_get_issuer(id1, &fields);
  printf("(id1) issuer = %s\n", fields.commonName);

  cssm_id_cert_get_subject(id1, &fields);
  printf("(id1) subject = %s\n", fields.commonName);

  printf("cssm_cert(publKeyCrt2 ...)\n");
  if (cssm_id_cert(id1, id2, 500)) {
    printf("error certifying id2: %s\n", cssm_last_error_msg);
  }
  // cssm_store_identity(id1, "id1_priv.pem", "id1_publ.pem", "id1_csr.pem", "id1_cert.pem", "id1_ca.pem");
  // cssm_store_identity(id2, "id2_priv.pem", "id2_publ.pem", "id2_csr.pem", "id2_cert.pem", "id2_ca.pem");

  printf("cssm_cert_get_issuer(id2, ...)\n");
  cssm_id_cert_get_issuer(id2, &fields);
  printf("(id2) issuer = %s\n", fields.commonName);

  cssm_id_cert_get_subject(id2, &fields);
  printf("(id2) subject = %s\n", fields.commonName);

  len = 512;
  if (cssm_id_sign(id2, msg, msgLen, (void**)&signature, &len)) {
    printf("(id2) error signing: %s\n", cssm_last_error_msg);
  }

  cssm_base64_encode(signature, len, signatureBase64, &signatureBase64Size);
  printf("(id2) msg = %s signed to %s (len=%lu) verify = %i\n", msg,
    signatureBase64, len, cssm_id_verify(id2, signature, len, msg, msgLen));

  const char *verificationError;
  int isCertValid = cssm_id_cert_verify(id2/* must contain the ca*/, &verificationError);
  printf("valid cert = %i (should be 1, errStr=\"%s\")\n", isCertValid, verificationError);

  msgLen = strlen(msg);
  cssm_id_sign(id2, msg, msgLen, (void**)&signature, &cipLen);

  printf("Message = %s\n", msg);
  
  len = 512;
  if (cssm_id_sign(id2, msg, msgLen, (void**)&signature, &len)) {
    printf("error signing: %s\n", cssm_last_error_msg);
  }
  cssm_base64_encode(signature, len, signatureBase64, &signatureBase64Size);

  printf("Signed message = %s (%zu B, verify = %i)\n", signatureBase64, len,
    cssm_id_verify(id2, signature, len, msg, msgLen));

  int isValid = cssm_id_cert_check_date_valid(id2);
  printf("Certificarte date is valid = %i\n", isValid);

  cssm_id_destroy_keys(id2);

  // -------------------------------
  // creates a new key and uses the previous as peer-key
  cssm_free_identity(id2);
  id2 = cssm_alloc_identity();
  cssm_id_create_keys(id2, 1, (cssm_csr_fields_t){
    .country = "PT",
    .state = "PT",
    .locality = "Lisbon",
    .org = "subjectOrg",
    .unit = "subjectOrgUnit",
    .commonName = "subject",
    .email = "subject@com.pt"
  });

  // void *eckey = NULL;
  // void *eckeyDes = NULL;
  char serializeEckey1[8192];
  char cipher1[8192];
  long cipher_size;
  // void *eckey2 = NULL; // repeat with new key, should give the same secret
  // void *eckeyDes2 = NULL;
  char serializeEckey2[8192];
  char decipher2[8192];
  long msg_size;

  cssm_identity_t *id1a = cssm_alloc_identity();
  cssm_identity_t *id2a = cssm_alloc_identity();

  // Node 1 creates a temporary key ...
  cssm_id_gen_ec_key(id1);
  // ... then serializes the public key and sends
  cssm_id_serialize_ec_pubkey(id1, (void*)serializeEckey1, 8192);
  
  // Same for Node 2
  cssm_id_gen_ec_key(id2);
  cssm_id_serialize_ec_pubkey(id2, (void*)serializeEckey2, 8192);
  
  // Node 1 deserializes N2 key
  cssm_id_deserialize_ec_pubkey(id2a, (void*)serializeEckey2, 8192);
  cssm_id_gen_peer_secret(id1, id2a);
  cssm_id_load_secret(id2a, NULL); // Now we can communicate N1->N2
  
  // Same for Node 2
  cssm_id_deserialize_ec_pubkey(id1a, (void*)serializeEckey1, 8192);
  cssm_id_gen_peer_secret(id2, id1a);
  cssm_id_load_secret(id1a, NULL);

  // id1a --> secret of N2 shared with N1
  // id2a --> secret of N1 shared with N2
  
  // N1->N2
  cssm_id_sym_cipher(id2a, msg, strlen(msg), cipher1, &cipher_size);
  cssm_id_sym_decipher(id1a, cipher1, cipher_size, decipher2, &msg_size);

  printf("msg = %s, decipher = %s\n", msg, decipher2);

  char hmac[1024];
  char hmacBase64[1024];
  long hmacSize;
  long hmacBase64Size;

  cssm_id_hmac(id2a, msg, strlen(msg), hmac, &hmacSize);
  cssm_base64_encode(hmac, hmacSize, hmacBase64, &hmacBase64Size);
  printf("msg = %s, hmac = %s (%zu Bytes)\n", msg, hmacBase64, hmacSize);

  cssm_free_identity(id1);
  cssm_free_identity(id2);
  cssm_free_identity(id1a);
  cssm_free_identity(id2a);

  cssm_destroy();

  return EXIT_SUCCESS;
}

