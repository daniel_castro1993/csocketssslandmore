#ifndef TCP_SOCKET_LIB_GUARD_
#define TCP_SOCKET_LIB_GUARD_

#define CSSM_MAX_HANDLERS        64
#define CSSM_HASH_SIZE           64 /* SHA512 */
#define CSSM_SIGNATURE_SIZE    1024 /* TODO */
#define CSSM_MAX_CONNECTIONS   1024
#define CSSM_MSG_QUEUE_SIZE      16
#define CSSM_HANDLER_THREADS      8
#define CSSM_MSG_BUFFER      262144 /* 256kB */
#define CSSM_MAX_SECRETS       1024
#define CSSM_CSR_FIELD_SIZE     128

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#include "handleError.h"

typedef struct cssm_csr_fields_t {
  char country   [CSSM_CSR_FIELD_SIZE];
  char state     [CSSM_CSR_FIELD_SIZE];
  char locality  [CSSM_CSR_FIELD_SIZE];
  char org       [CSSM_CSR_FIELD_SIZE];
  char unit      [CSSM_CSR_FIELD_SIZE];
  char commonName[CSSM_CSR_FIELD_SIZE];
  char email     [CSSM_CSR_FIELD_SIZE];
} cssm_csr_fields_t;

typedef struct cssm_identity_t cssm_identity_t;

typedef void(*cssm_handler_t)(void*,long,void(*respondWith)(void*,long),void(*waitResponse)(void*, long*)); 

// if isServerSet returns -2 server is already running
int cssm_init(char *port); // port == NULL --> any port
int cssm_check_port(); // returns the port the server is bind to
int cssm_destroy();

/**
 * Connects to <address>:<port>
 * returns:  0 // no error
 *          -1 // max number of connections reached
 *          -2 // error address format
 *          -3 // error openning socket or connecting
 **/
int cssm_connect_to(char *addr, char *port); // prints connection count

int cssm_close_all_connections(); // closes all connections
int cssm_send_msg(int connId, void *msg, long);
int cssm_recv_msg(int connId, void *msg, long*);

// NOTE: handlers are executed sequentially
int cssm_add_handler(cssm_handler_t);

cssm_identity_t *cssm_alloc_identity();
void cssm_free_identity(cssm_identity_t *identity);

// return -1 for error
int cssm_store_identity(
  cssm_identity_t *identity,
  const char *priv_key,
  const char *publ_key,
  const char *cert_req,
  const char *cert,
  const char *ca
);

// return -1 for error, 0 no error,
// 1 could not open priv (can be in any bit combination)
// 2 could not open publ
// 4 could not open csr
// 8 could not open cert
// 16 could not open ca
int cssm_load_identity(
  cssm_identity_t *identity,
  const char *priv_key,
  const char *publ_key,
  const char *cert_req,
  const char *cert,
  const char *ca
);
int cssm_id_create_keys(
  cssm_identity_t *identity,
  int secStrength /* 1 - weakest, 3 - strongest */,
  cssm_csr_fields_t fields
);
int cssm_id_destroy_keys(cssm_identity_t *identity);
int cssm_id_cert_get_issuer(cssm_identity_t*, cssm_csr_fields_t*); // load first
int cssm_id_cert_get_subject(cssm_identity_t*, cssm_csr_fields_t*); // load first
int cssm_id_cert_check_date_valid(cssm_identity_t*); // load first, returns 1 on valid, 0 not valid, -1 error
int cssm_id_create_self_signed_cert(cssm_identity_t*, long daysValid, cssm_csr_fields_t subject);
int cssm_id_cert(cssm_identity_t *ca, cssm_identity_t *csr, long daysValid);
int cssm_id_set_ca(cssm_identity_t *cert, cssm_identity_t *ca);
int cssm_id_cert_verify(cssm_identity_t *cert, const char **err_str);

// NOTE: programmer must give use buffers of size CSSM_MSG_BUFFER,
// if it needs more space than that it returns -1
int cssm_id_sign(
  cssm_identity_t*,
  void *data,       long dlen,
  void *ciphertext, long *clen
); // clen --> must containt the size of the buffer

// checks with public key
int cssm_id_verify(
  cssm_identity_t*,
  void *ciphertext, long clen,
  void *data,       long dlen
); // -1 is an error, 1 is ok, 0 is false

// secret sharing methods

// generates ephemeral key
int cssm_id_gen_ec_key(cssm_identity_t *id);
// serializes the ephemeral (public) key, after this, send the buffer to the peer, returns space taken
long cssm_id_serialize_ec_pubkey(cssm_identity_t *id, void *buffer, long size);
// deserialize ephemeral key from peer
int cssm_id_deserialize_ec_pubkey(cssm_identity_t *peer, void *buffer, long size); // pass &eckey, where eckey is void* = NULL
// after generated and received the ephemeral keys, creates a secret
int cssm_id_gen_peer_secret(cssm_identity_t *id, cssm_identity_t *peer); // returns the ID of the key (up to CSSM_MAX_SECRETS)

// pass secret = NULL to load the ec secret
int cssm_id_load_secret(cssm_identity_t *id, char *secret);

int cssm_id_sym_cipher(cssm_identity_t *id, void *data, long, void *ciphertext, long*);
int cssm_id_sym_decipher(cssm_identity_t *id, void *ciphertext, long, void *data, long*);
int cssm_id_hmac(cssm_identity_t *id, void *data, long dlen, void *out, long *olen); // uses loaded key

int cssm_hash(void *data, long dlen, void *hashed, long *hlen);

int cssm_base64_decode(char *data, long dlen, char *out, long *olen);
int cssm_base64_encode(char *data, long dlen, char *out, long *olen);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TCP_SOCKET_LIB_GUARD_ */
