#ifndef TCP_SERVER_H_GUARD
#define TCP_SERVER_H_GUARD

#include "cssm.h"

#include <string.h>
#include <string>
#include <map>
#include <list>
#include <exception>

namespace cssm
{

class CSSMError : public std::exception
{
  virtual const char* what() const throw()
  {
    return (const char*)cssm_last_error_msg;
  }
};

class Message
{
public:
  Message() 
    : m_header(NULL),
      m_contents(NULL),
      m_buffer(NULL),
      m_header_size(0),
      m_alloc_header_size(0),
      m_contents_size(0),
      m_alloc_contents_size(0),
      m_buffer_size(0)
    { InitBuffer(); };
  Message(int isRecvMsg, unsigned char *buffer, long len) : Message()
    { 
      if (isRecvMsg)
        SetBuffer(buffer, len);
      else 
        SetContents(buffer, len);
    };
  Message(const char *buffer)
    : Message(0, (unsigned char *)buffer, strlen(buffer)) { };
  Message(const Message &msg) : Message(1, msg.m_buffer, msg.m_buffer_size)
    { };
  ~Message()
  {
    if (m_header)   free(m_header );
    if (m_contents) free(m_contents);
    if (m_buffer)   free(m_buffer);
  };

  // custom type to tag the message
  void SetType(unsigned char type /* 1B */);
  // Note: the buffer will be part of the message, must be managed outside
  int SetContents(unsigned char *contents, long size);
  int SetBuffer(unsigned char *buffer, long size);

  unsigned char GetType();
  unsigned char *GetContents(long &size);
  unsigned char *GetBuffer(long &size); // buffer with header+contents (does memcpy's)

  int Sign(cssm_identity_t *id);
  int Verify(cssm_identity_t *peer);
  int HmacSign(cssm_identity_t *id);
  int HmacVerify(cssm_identity_t *peer);
  int Cipher(cssm_identity_t *id);
  int Decipher(cssm_identity_t *id);

  int RespondWith(Message &msg);
  Message WaitResponse();

  // internal use only
  int SetRespondWith(void(*respondWith)(void*,long));
  int SetWaitResponse(void(*waitResponse)(void*,long*));

private:
  void InitBuffer();

  unsigned char *m_header;
  unsigned char *m_contents; // contents
  unsigned char *m_buffer; // contains all that is sent
  long m_header_size;
  long m_alloc_header_size;
  long m_contents_size;
  long m_alloc_contents_size;
  long m_buffer_size;
  void(*m_respondWith)(void*,long);
  void(*m_waitResponse)(void*, long*);
};

class IEntity
{
public:
  IEntity() :
    m_name("local"),
    m_localPrivKeys(m_name + "_key_priv/"),
    m_localPublKeys(m_name + "_key_publ/"),
    m_addr("localhost"),
    m_port(-1),
    m_id(cssm_alloc_identity())
  { 
    memset((void*)&m_iss, 0, sizeof(m_iss));
    memset((void*)&m_sub, 0, sizeof(m_sub));
    LoadId(); // can be repeated safely
  };

  IEntity(const IEntity &e) :
    m_name(e.m_name),
    m_localPrivKeys(e.m_localPrivKeys),
    m_localPublKeys(e.m_localPublKeys),
    m_addr(e.m_addr),
    m_port(e.m_port),
    m_id(cssm_alloc_identity()) // does not copy
  { 
    memcpy((void*)&m_iss, (void*)&e.m_iss, sizeof(m_iss)); 
    memcpy((void*)&m_sub, (void*)&e.m_sub, sizeof(m_sub)); 
    LoadId();
  };

  IEntity(void *buffer, long len) : IEntity()
    { cssm_id_deserialize_ec_pubkey(m_id, buffer, len); };

  IEntity(std::string name) : IEntity()
  {
    m_name = name;
    m_localPrivKeys = m_name + "_key_priv/"; 
    m_localPublKeys = m_name + "_key_publ/";
  };

  IEntity(std::string name, std::string addr, int port) : IEntity(name)
    { m_addr = addr; m_port = port; };

  // only non-copied object is allowed to free: causes SIGSEGV if top object free'd first
  ~IEntity() { if (m_id) { cssm_free_identity(m_id); } } ;

  void SetLocalPrivKeys(std::string localPrivKeys) 
    { m_localPrivKeys = localPrivKeys; }
  void SetLocalPublKeys(std::string localPublKeys) 
    { m_localPublKeys = localPublKeys; }

  std::string GetName() { return m_name; }
  std::string GetAddr() { return m_addr; }
  int GetPort() { return m_port; }

  cssm_identity_t *GetId() { return m_id; }
  void SerlKey(void *buf, long *l)
  {
    cssm_err_flag = 0;
    *l = cssm_id_serialize_ec_pubkey(m_id, buf, *l);
    if (cssm_err_flag) throw CSSMError();
  }
  void DeserlKey(void *buffer, long len)
    { if (cssm_id_deserialize_ec_pubkey(m_id, buffer, len)) throw CSSMError(); }

  void SetIssuerCountry(std::string c)      
    { memcpy(m_iss.country,    (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetIssuerState(std::string c)        
    { memcpy(m_iss.state,      (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetIssuerLocal(std::string c)        
    { memcpy(m_iss.locality,   (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetIssuerCommonName(std::string c)   
    { memcpy(m_iss.commonName, (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetIssuerOrganization(std::string c) 
    { memcpy(m_iss.org,        (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetIssuerOrgUnit(std::string c)      
    { memcpy(m_iss.unit,       (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };

  void SetSubjectCountry(std::string c)      
    { memcpy(m_sub.country,    (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetSubjectState(std::string c)        
    { memcpy(m_sub.state,      (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetSubjectLocal(std::string c)        
    { memcpy(m_sub.locality,   (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetSubjectCommonName(std::string c)   
    { memcpy(m_sub.commonName, (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetSubjectOrganization(std::string c) 
    { memcpy(m_sub.org,        (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };
  void SetSubjectOrgUnit(std::string c)      
    { memcpy(m_sub.unit,       (void*)c.c_str(), strnlen(c.c_str(), 127)+1); };

  void CreateKeyPair(int secStrength)
  {
    cssm_id_create_keys(m_id, secStrength, m_sub); 
    cssm_id_create_self_signed_cert(m_id, 9999, m_sub);
  }
  void CertKey(IEntity &e, long days)
  {
    cssm_id_cert(m_id, e.m_id, days);
  }

  int VerifyId(const char *ca_cert_path);
  int VerifyId(cssm_identity_t *ca);

  int StoreId();
  int LoadId();

protected:
  std::string m_name; 
  std::string m_localPrivKeys;
  std::string m_localPublKeys;
  std::string m_addr; 
  int m_port;
  cssm_identity_t *m_id;
  cssm_csr_fields_t m_iss;
  cssm_csr_fields_t m_sub;
};

class Entity : public IEntity
{
public:

  Entity() { };
  Entity(void *buffer, long len) : IEntity(buffer, len) { };
  Entity(const IEntity &e) : IEntity(e) { };
  Entity(std::string name) : IEntity(name) { };
  Entity(std::string name, std::string addr, int port) : IEntity(name, addr, port) { };
  ~Entity() { };

};

class RemoteEntity : public IEntity
{
public:

  RemoteEntity() : IEntity(), m_pairKey(NULL) { };
  RemoteEntity(const IEntity &e) : IEntity(e), m_pairKey(NULL) { };
  RemoteEntity(std::string name) : IEntity(name), m_pairKey(NULL) { };
  RemoteEntity(void *buffer, long len) 
    : IEntity(buffer, len), m_pairKey(NULL) { };
  RemoteEntity(std::string name, std::string addr, int port) 
    : IEntity(name, addr, port), m_pairKey(NULL) { };
  ~RemoteEntity() { if (m_pairKey) cssm_free_identity(m_pairKey); };

  // public key and/or cert must be in LOCAL_KEYS/m_name.*
  int PairWithKey(cssm_identity_t *pair);
  int CipherMsg(Message &msg);
  int DecipherMsg(Message &msg);
  int SignMsg(Message &msg);
  int VerifyMsg(Message &msg);
  int HmacSignMsg(Message &msg);
  int HmacVerityMsg(Message &msg);

private:
  cssm_identity_t *m_pairKey;
};

static const char *TSLConnErrorStrs[5] =
{
  "No error",
  "Out of memory for connections",
  "Wrong address format",
  "Recipient socket is not listening",
  "Unknown connection error"
};

struct ConnectionStats
{
public:
  RemoteEntity m_remote;
  float m_lastLatency;
  float m_avgLatency;
  long m_nbMeasures;
  int m_lastError;
};

class TSLConnError : public std::exception
{
public:
  TSLConnError() : m_error(0) { };
  TSLConnError(int error) { m_error = (error < 0 || error > 4) ? 4 : error; };

  virtual const char* what() const throw()
  {
    return (const char*)TSLConnErrorStrs[m_error];
  }

  int m_error;
};

class Connection
{
public:
  Connection(Entity &local) : m_local(local), m_currentConn(-1) { };
  ~Connection() { };

  int ConectTo(RemoteEntity re); // stores info of connection
  int ConectTo(std::string name);

  int SendMsg(Message &msg);

private:
  Entity &m_local;
  int m_currentConn;
  // name, entity
  std::map<std::string, RemoteEntity> m_conn;
  std::map<std::string, ConnectionStats> m_stat; // TODO
};

typedef int(*ProcessMsg_t)(Message &msg);

class Server
{
public:
  Server() : m_port(-1), m_conn(m_anon), m_anon() { Start(); }; // random port
  Server(Entity &id) : m_port(-1), m_conn(id) { Start(); };
  Server(Entity &id, int port) : m_port(port), m_conn(id) { Start(); };
  ~Server() { }

  int AddCallback(unsigned char msgType, ProcessMsg_t clbk);
  int SendMsg(RemoteEntity re, Message &msg);
  int SendMsg(std::string name, Message &msg);
  int SendMsg(Message &msg); // uses last connection
  int GetPort() { return m_port; }

private:

  int Start();

  int m_port;
  Connection m_conn;
  Entity m_anon;
  std::map<unsigned char, std::list<ProcessMsg_t>> m_clbks;
};



}

#endif /* TCP_SERVER_H_GUARD */
