#ifndef HANDLE_ERROR_H_GUARD_
#define HANDLE_ERROR_H_GUARD_

#include <stdio.h>

#define CSSM_ADD_ERROR(...) \
  cssm_err_flag = 1; sprintf(cssm_last_error_msg, __VA_ARGS__) \
\
//

extern __thread int cssm_err_flag;
extern __thread char cssm_last_error_msg[1024];

#endif /* HANDLE_ERROR_H_GUARD_ */