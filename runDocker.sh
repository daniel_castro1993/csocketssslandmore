#!/bin/bash

docker build -t cssm:ubuntu -f Dockerfile.ubuntu .

docker run --rm -it \
    -v $(pwd):/home/cssm \
    --add-host="localhost:127.0.0.1" \
    cssm:ubuntu
