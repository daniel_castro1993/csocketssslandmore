#include "prod-cons.h"
#include "threading.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>

static void callback(void *arg, int idx) {
  printf("Hello from thread %i! (%p)\n", idx, arg);
}

// static void callback2(int id, int nb_thrs, void *arg) {
//   for (int i = 0; i < 5; ++i) {
//     printf("%i/%i hello %i!\n", id, nb_thrs, i);
//   }
// }

static void callback3(int id, int nb_thrs, void *arg) {
  printf("callback3 called (%i/%i) from parent %li\n", id, nb_thrs, (intptr_t)arg);

  for (int i = 0; i < 5; ++i) {
    printf("%i/%i (p%li) hello callback3 %i!\n", id, nb_thrs, (intptr_t)arg, i);
  }
}

static void callback4(int id, int nb_thrs, void *args) {
  printf("callback4 from thread %i/%i launches thread with callback3\n", id, nb_thrs);
  threading_start(2, 0, callback3, (void*)(intptr_t)id);
}

int main ()
{
  prod_cons_start_thread();

  // sends three requests, note the requests are serialized
  for (long i = 0; i < 0x20; ++i) {
    prod_cons_async_request((prod_cons_async_req_s){
      .args = (void*)i,
      .fn = callback
    }, 0);
  }

  prod_cons_join_threads();

  printf(" --------------------------------------- \n");

  int id1, id2;
  id1 = prod_cons_start_thread(); // allocates two
  id2 = prod_cons_start_thread();

  assert(id1 == 0);
  assert(id2 == 1);

  // sends three requests, note the requests are serialized
  for (long i = 0; i < 0x20; ++i) {
    prod_cons_async_request((prod_cons_async_req_s){
      .args = (void*)(i + id1*0x20),
      .fn = callback
    }, id1);
    prod_cons_async_request((prod_cons_async_req_s){
      .args = (void*)(i + id2*0x20),
      .fn = callback
    }, id2);
  }

  prod_cons_join_threads();

  printf(" --------------------------------------- \n");

  threading_start(2, 0, callback4, NULL);

  threading_join();

  return EXIT_SUCCESS;
}
