#include "prod-cons.h"
#include "threading.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <thread>
#include <map>
#include <list>
#include <vector>
#include <mutex>

#include "util.h"

using namespace std;

typedef struct thr_info_ {
  int tid;
  int nbThreads;
  void *args;
  threading_callback fn;
} thr_info_s;

static vector<THREAD_T> async_thread;
static vector<THREAD_T> threads;
static vector<thr_info_s> info_thrs;
static vector<int> is_thread_active;
static THREAD_T timer_thread;
static int nbAsyncThrs = 0, allocedAsyncThrs = 0;
static int nbThrsExec = 0, isExit = 0, isTimerOn = 0;
static thread_local threading_id myThreadID;

static mutex timer_lock, start_lock;
static map<uint64_t, pair<threading_timer_callback, void*>> timer_fns;

static void *thrClbk(void *args);
static void *timerClbk(void *args);
static int msleep(uint64_t msec);

int prod_cons_start_thread()
{
  intptr_t pdIdx = prod_cons_init_main_thread();
  if (pdIdx != -1 && nbAsyncThrs < MAX_PROD_CONS_THRS) {
    if (async_thread.size() == 0) {
      async_thread.resize(MAX_PROD_CONS_THRS);
      allocedAsyncThrs = MAX_PROD_CONS_THRS;
    }
    thread_create_or_die
    ( async_thread[nbAsyncThrs],
      /* attr */    NULL,
      /* callback */prod_cons_main_thread,
      /* args */    (void*)pdIdx
    );
    nbAsyncThrs++; // not thread-safe
  }
  return pdIdx;
}

int prod_cons_join_threads()
{
  prod_cons_stop_all_threads();
  for (int i = 0; i < nbAsyncThrs; ++i) {
    thread_join_or_die(async_thread[i], NULL);
  }
  prod_cons_destroy_all_threads();
  nbAsyncThrs = 0;
  return 0;
}

void
threading_start
( int                nbThreads,
  int                asyncs,
  threading_callback callback,
  void              *args
) {
  start_lock.lock();
  int oldNbThrs = nbThrsExec;
  nbThrsExec += nbThreads;
  // malloc_or_die(async_thread, asyncs); // done in prod_cons_start_thread()
  threads.resize(nbThrsExec);
  info_thrs.resize(nbThrsExec);
  is_thread_active.resize(nbThrsExec);
  // realloc_or_die(threads, nbThrsExec);
  // realloc_or_die(info_thrs, nbThrsExec);
  // realloc_or_die(is_thread_active, nbThrsExec);

  for (int i = oldNbThrs; i < nbThrsExec; ++i) {
    is_thread_active[i] = 0;
    info_thrs[i].tid = i - oldNbThrs;
    info_thrs[i].nbThreads = nbThreads;
    info_thrs[i].args = args;
    info_thrs[i].fn = callback;
    thread_create_or_die(threads[i], NULL, thrClbk, &(info_thrs[i]));
  }

  if (nbAsyncThrs == 0) {
    for (int i = 0; i < asyncs; ++i) {
      int pcIdx = prod_cons_start_thread();
      if (pcIdx == -1) {
        fprintf(stderr, "Could not launch all the requested asyncs\n");
        exit(EXIT_FAILURE);
      }
    }
  }

  // timer
  if (!isTimerOn) {
    isTimerOn = 1;
    thread_create_or_die(timer_thread, NULL, timerClbk, NULL);
  }
  start_lock.unlock();
}

void threading_join()
{
  int readNbThrsExec = nbThrsExec;
  __atomic_store_n(&isExit, 1, __ATOMIC_RELEASE);
join:
  thread_join_or_die(timer_thread, NULL);
  for (int i = 0; i < readNbThrsExec; ++i) {
    thread_join_or_die(threads[i], NULL);
    readNbThrsExec = __atomic_load_n(&nbThrsExec, __ATOMIC_ACQUIRE);
  }
  prod_cons_join_threads();
  start_lock.lock();
  if (__atomic_load_n(&nbThrsExec, __ATOMIC_ACQUIRE) != readNbThrsExec) {
    readNbThrsExec = __atomic_load_n(&nbThrsExec, __ATOMIC_ACQUIRE);
    start_lock.unlock();
    goto join;
  }
  // free(threads);      threads = NULL;
  // free(info_thrs);    info_thrs = NULL;
  // free(async_thread); async_thread = NULL;
  nbThrsExec = 0;
  isTimerOn = 0;
  allocedAsyncThrs = 0;
  start_lock.unlock();
}

int threading_async(int idAsync, prod_cons_req_fn fn, void *args)
{
  prod_cons_async_request((prod_cons_async_req_s){
    .args = args,
    .fn = fn
  }, idAsync);
  return 0;
}

int threading_getMaximumHardwareThreads()
{
  return std::thread::hardware_concurrency(); // C++11
}

void threading_pinThisThread(int coreID)
{
  THREAD_PIN_TO_CORE(coreID);
}

int threading_getNbThreads()
{
  return nbThrsExec;
}

threading_id threading_getMyThreadID()
{
  return myThreadID;
}

int threading_timer(int millis, threading_timer_callback fn, void *arg)
{
  struct timespec now;
  uint64_t now_ms;

  if (millis < 0) return -1;

  clock_gettime(CLOCK_MONOTONIC_RAW, &now);
  now_ms = now.tv_sec * 1000 + now.tv_nsec / 1000000;
  now_ms += millis;
  timer_lock.lock();
  timer_fns.insert(make_pair(now_ms, make_pair(fn, arg)));
  timer_lock.unlock();

  return 0;
}

static void *thrClbk(void *args)
{
  thr_info_s *info = (thr_info_s*)args;
  myThreadID = info->tid;
  info->fn(info->tid, info->nbThreads, info->args);
  return NULL;
}

static void *timerClbk(void *args)
{
  // clock_getres(CLOCK_MONOTONIC_RAW, &timer_resolution); // TODO: check resolution
  // CLOCKS_PER_SEC
  struct timespec now;
  uint64_t now_ms;

  while (!isExit)
  {
    map<threading_timer_callback, void*> fns;
    list<uint64_t> toErase;

    clock_gettime(CLOCK_MONOTONIC_RAW, &now);
    now_ms = now.tv_sec * 1000 + now.tv_nsec / 1000000;

    timer_lock.lock();
    for (auto it = timer_fns.begin(); it != timer_fns.end(); ++it) {
      // stuff waiting on the timer
      if (it->first < now_ms) {
        auto fn = it->second.first;
        auto arg = it->second.second;
        toErase.push_back(it->first);
        fns.insert(make_pair(fn, arg));
      } else {
        break; // assumes ordered map
      }
    }
    for (auto it = toErase.begin(); it != toErase.end(); ++it) {
      timer_fns.erase(*it);
    }
    timer_lock.unlock();

    for (auto it = fns.begin(); it != fns.end(); ++it) {
      auto fn = it->first;
      auto arg = it->second;
      fn(arg); // executes the timer function
    }
    //do stuff
    msleep(1);
  }
  return NULL;
}

static int msleep(uint64_t msec)
{
  struct timespec ts;
  int res;

  ts.tv_sec = msec / 1000;
  ts.tv_nsec = (msec % 1000) * 1000000;

  do {
    res = nanosleep(&ts, &ts);
  } while (res && errno == EINTR);

  return res;
}
