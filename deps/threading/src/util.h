#ifndef UTIL_H_GUARD
#define UTIL_H_GUARD

#define malloc_or_die(_var, _nb) \
    if (((_var) = (__typeof__((_var)))malloc((_nb) * sizeof(__typeof__(*(_var))))) == NULL) { \
      fprintf(stderr, "malloc error \"%s\" at " __FILE__":%i\n", \
      strerror(errno), __LINE__); \
      exit(EXIT_FAILURE); \
    } \
//#define malloc_or_die

#define realloc_or_die(_var, _nb) \
    if (((_var) = (__typeof__((_var)))realloc((_var), (_nb) * sizeof(__typeof__(*(_var))))) == NULL) { \
      fprintf(stderr, "realloc error \"%s\" at " __FILE__":%i\n", \
      strerror(errno), __LINE__); \
      exit(EXIT_FAILURE); \
    } \
//#define realloc_or_die

#define thread_create_or_die(_thr, _attr, _callback, _arg) \
    if (THREAD_CREATE(_thr, _attr, _callback, (void *)(_arg)) != 0) { \
      fprintf(stderr, "Error creating thread at " __FILE__ ":%i\n", __LINE__); \
      exit(EXIT_FAILURE); \
    } \
//#define thread_create_or_die

#define thread_join_or_die(_thr, _res) \
    if (THREAD_JOIN(_thr, _res)) { \
      fprintf(stderr, "Error joining thread at " __FILE__ ":%i\n", __LINE__); \
      exit(EXIT_FAILURE); \
    } \
//#define thread_join_or_die

#if defined(USE_CSTD)
#include <threads.h>

#define THREAD_T               thrd_t
#define THREAD_T_INIT(_thrd_t) /*do nothing*/
#define THREAD_RET_T void*
#define THREAD_ARG_T void*
#define THREAD_SUCCESS_RET NULL
#define THREAD_FAILURE_RET ((THREAD_RET_T)-1)
typedef THREAD_RET_T(*THREAD_CALLBACK)(THREAD_ARG_T);
#define THREAD_CREATE(_thr_t, _attrs, _callbk, _args) \
  thrd_create(NULL, 0, _callbk, _args, /* TODO */0, &(_thr_t))
#define THREAD_JOIN(_thr_t, _ret) \
  thrd_join(_thr_t, /* TODO */0, _res);

#elif defined(_WIN32) || defined(_WIN64)

// TODO

#elif defined(__APPLE__)
#include <semaphore.h>

//#define THRD_SEM_T           sem_t*
typedef sem_t* THRD_SEM_T;
#define THRD_SEM_INIT(s, n)  s = sem_open("thrSem", O_CREAT | O_EXCL, \
                            S_IRUSR | S_IWUSR, n); if (s != SEM_FAILED) { \
                            sem_unlink("thrSem"); }
#define THRD_SEM_DESTROY(s)  sem_close(s)
#define THRD_SEM_POST(s)     sem_post(s)
#define THRD_SEM_WAIT(s)     sem_wait(s)

#include <sys/types.h>
#include <sys/sysctl.h>
#include <mach/mach_init.h>
#include <mach/thread_policy.h>
#include <mach/thread_act.h>
#include <pthread.h>

// mac os also need to re-implement some functions
#define SYSCTL_CORE_COUNT   "machdep.cpu.core_count"

typedef struct cpu_set {
  uint32_t    count;
} cpu_set_t;

static inline void
CPU_ZERO(cpu_set_t *cs) { cs->count = 0; }

static inline void
CPU_SET(int num, cpu_set_t *cs) { cs->count |= (1 << num); }

static inline int
CPU_ISSET(int num, cpu_set_t *cs) { return (cs->count & (1 << num)); }

static inline int
sched_getaffinity(pid_t pid, size_t cpu_size, cpu_set_t *cpu_set)
{
  int32_t core_count = 0;
  size_t  len = sizeof(core_count);
  int ret = sysctlbyname(SYSCTL_CORE_COUNT, &core_count, &len, 0, 0);
  if (ret) {
    printf("error while get core count %d\n", ret);
    return -1;
  }
  cpu_set->count = 0;
  for (int i = 0; i < core_count; i++) {
    cpu_set->count |= (1 << i);
  }

  return 0;
}

static inline int
pthread_setaffinity_np(
  pthread_t thread, size_t cpu_size, cpu_set_t *cpu_set
) {
  thread_port_t mach_thread;
  int core = 0;

  for (core = 0; core < 8 * cpu_size; core++) {
    if (CPU_ISSET(core, cpu_set)) break;
  }
  printf("binding to core %d\n", core);
  thread_affinity_policy_data_t policy = { core };
  mach_thread = pthread_mach_thread_np(thread);
  thread_policy_set(mach_thread, THREAD_AFFINITY_POLICY,
                    (thread_policy_t)&policy, 1);
  return 0;
}

#define THREAD_T               pthread_t
#define THREAD_T_INIT(_thrd_t) /*do nothing*/
#define THREAD_RET_T void*
#define THREAD_ARG_T void*
#define THREAD_SUCCESS_RET NULL
#define THREAD_FAILURE_RET ((THREAD_RET_T)-1)
typedef THREAD_RET_T(*THREAD_CALLBACK)(THREAD_ARG_T);
#define THREAD_CREATE(_thr_t, _attrs, _callbk, _args) \
  pthread_create(&(_thr_t), _attrs, _callbk, _args)
#define THREAD_JOIN(_thr_t, _ret) \
  pthread_join(_thr_t, _ret)

#define THREAD_PIN_TO_CORE(_core_id) ({ \
  cpu_set_t _cpu_set; \
  CPU_ZERO(&_cpu_set); \
  CPU_SET(_core_id, &_cpu_set); \
  pthread_setaffinity_np(0, sizeof(cpu_set_t), &_cpu_set); \
})

#elif defined(__linux__)
#include <semaphore.h>
#include <pthread.h>
#include <sched.h>

#define THREAD_T               pthread_t
#define THREAD_T_INIT(_thrd_t) /*do nothing*/
#define THREAD_RET_T void*
#define THREAD_ARG_T void*
#define THREAD_SUCCESS_RET NULL
#define THREAD_FAILURE_RET ((THREAD_RET_T)-1)
typedef THREAD_RET_T(*THREAD_CALLBACK)(THREAD_ARG_T);
#define THREAD_CREATE(_thr_t, _attrs, _callbk, _args) \
  pthread_create(&(_thr_t), _attrs, _callbk, _args)
#define THREAD_JOIN(_thr_t, _ret) \
  pthread_join(_thr_t, _ret)

#define THREAD_PIN_TO_CORE(_core_id) ({ \
  cpu_set_t _cpu_set; \
  CPU_ZERO(&_cpu_set); \
  CPU_SET(_core_id, &_cpu_set); \
  sched_setaffinity(0, sizeof(cpu_set_t), &_cpu_set); \
})

#define THRD_SEM_T           sem_t
#define THRD_SEM_INIT(s, n)  sem_init(&s, 0, n)
#define THRD_SEM_DESTROY(s)  sem_destroy(&s)
#define THRD_SEM_POST(s)     sem_post(&s)
#define THRD_SEM_WAIT(s)     sem_wait(&s)

#else  /* !__APPLE__ && !__linux__ */
#error "No threading lib available"

// TODO: need to define thread lib
#define THREAD_T /* empty */
typedef void*(*thr_callbck)(void*);
#define THREAD_CREATE(_thr_t, _attrs, _callbk, _args) /* empty */
#define THREAD_JOIN(_thr_t, _ret)

#endif /* __APPLE__ */

#endif /* UTIL_H_GUARD */
