OBJS     := \
	src/tcpSocketLib.o \
	src/sslUtility.o \
	src/tcpServer.o \
#
OBJS_DEB := \
	src/tcpSocketLib_DEB.o \
	src/sslUtility_DEB.o \
	src/tcpServer_DEB.o \
#

LIBS     := \
	-L ./deps/threading -l threading \
	-L ./deps/input_handler -l input_handler \
	-L /usr/local/opt/openssl/lib -l ssl -l crypto \
	-pthread 
INCS     := -I ./include -I ./deps/threading/include -I ./deps/input_handler/include
DEFS     := -Wall -Wcpp

CC       := gcc -std=c11
CXX      := g++ -std=c++17
AR       := ar rcs

DEBUG_DEF:= -O0 -g
OPT_DEF  := -O3

LIB      := cssm

CFLAGS   := -c $(DEFS) $(INCS)
CXXFLAGS := -c $(DEFS) $(INCS)

LDFLAGS  := -L . -l $(LIB) $(LIBS)
LDFLAGS_DEB := -L . -l $(LIB)_DEB $(LIBS)

all: lib$(LIB).a lib$(LIB)_DEB.a test/test test/test_ssl test/test_sockets
	cd ./deps/threading     && $(MAKE) 
	cd ./deps/input_handler && $(MAKE) 
	# DONE

deps: threading input_handler

threading:
	cd ./deps/threading && $(MAKE)

input_handler:
	cd ./deps/input_handler && $(MAKE)

lib$(LIB).a: deps $(OBJS)
	@echo "Linking..."
	$(AR) $@ $(OBJS)

lib$(LIB)_DEB.a: deps $(OBJS_DEB)
	@echo "Linking..."
	$(AR) $@ $(OBJS_DEB)

test/test: $(OBJS_DEB) lib$(LIB)_DEB.a test/test_DEB.o
	$(CXX) $^ $(LDFLAGS_DEB) -o $@

test/test_ssl: $(OBJS_DEB) lib$(LIB)_DEB.a test/test_ssl_DEB.o
	$(CXX) $^ $(LDFLAGS_DEB) -o $@

test/test_sockets: $(OBJS_DEB) lib$(LIB)_DEB.a test/test_sockets_DEB.o
	$(CXX) $^ $(LDFLAGS_DEB) -o $@

%.o:	%.c
	@echo "--- $<"
	$(CC) $(CFLAGS) $(OPT_DEF) -o $@ $<

%_DEB.o:	%.c
	@echo "--- $<"
	$(CC) $(CFLAGS) $(DEBUG_DEF) -o $@ $<

%.o:	%.cpp
	@echo "--- $<"
	$(CXX) $(CXXFLAGS) $(OPT_DEF) -o $@ $<

%_DEB.o:	%.cpp
	@echo "--- $<"
	$(CXX) $(CXXFLAGS) $(DEBUG_DEF) -o $@ $<

clean: clean_deps clean_apps
	rm -f $(OBJS) $(OBJS_DEB) $(TEST_OBJS) lib$(LIB).a lib$(LIB)_DEB.a test/test test/test_ssl test/test_sockets test/*.o

clean_deps:
	cd ./deps/threading     && $(MAKE) clean
	cd ./deps/input_handler && $(MAKE) clean

clean_apps:
	cd ./apps/clock_server && $(MAKE) clean
	cd ./apps/heartbeat    && $(MAKE) clean
	cd ./apps/key_manager  && $(MAKE) clean
